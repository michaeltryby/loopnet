# Building Loopnet

The simplest way to build Loopnet is by using `CMake` (https://cmake.org/). You will need `CMake` and a C++ compiler to build Loopnet. It can be built on Linux/Mac and Windows.

To build Loopnet on **Linux/Mac**, first `cd` to the base directory of Loopnet, then run the commands:

```
mkdir build
cmake ..
make
```

Alternatively, you can use `cmake-gui` instead of `cmake`.

To build Loopnet on **Windows**:

1. Run `cmake-gui`.
2. Select the base directory of Loopnet (the folder containing the file `CMakeLists.txt`) as the source folder. Create a subfolder of it and select it as the build folder.
2. Press "Configure". Select the "generator", i.e. a compiler environment installed on your computer that you want to use to compile the project, e.g. "Visual Studio 15 2017". Depending on the compiler option you choose, 32-bit or 64-bit code will be generated (e.g. "Visual Studio 15 2017" generates 32-bit code, while "Visual Studio 15 2017 Win64" yields 64-bit code).
3. Modify build options (optional, see below), then press "Generate". This will have prepared things for the actual compilation, generating project file(s) in the build folder.
4. Press "Open Project" to open the project with the selected Compiler Environment, then compile it.

## Build options

You can provide different build options by supplying arguments to the `cmake` command or by using `cmake-gui`. The most important build options are:

- `CMAKE_BUILD_TYPE`: Set this variable to `Release` to generate fastest code (the normal case). Set it to `Debug` to generate code with debug information. Some compiler environments (e.g. Visual Studio) allow multiple values separated by semicolons (e.g. `Debug;Release;MinSizeRel;RelWithDebInfo`).
- `LOOPNET_DLL`: If this variable is `ON` (the default), a Dynamic Link Library toolkit (`libloopnet.dll` in Windows or `libloopnetlib.so` in Linux) will be generated, containing the functions that can be used in your own programs. A command-line executable (`loopnet.exe` in Windows or `loopnet` in Linux) will also be built.
If this variable is `OFF`, only the command-line executable is generated.
- `OPENMP`: Enable OpenMP parallelization. If `ON` (default), Loopnet will use all CPU cores. Otherwise, it will use a single core.
- `PRVMETH_SYM`: If `ON` (default), use symmetric matrix formulation for PRVs/PSVs.
- `TIMING`: Level of detail for time measuring (time info is printed in the report file):
    - 0: No time measuring
    - 1: (default) Minimal time measuring
    - 2: Detailed time measuring
