/*
*********************************************************************
                                                                   
 LOOPNET -- Hydraulic & Water Quality Simulator for Water Distribution Networks
 See LICENSE and AUTHORS files

 RESIDUALS.C -- compute residuals of the hydraulic non-linear system

*******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "types.h"
#include "funcs.h"
#define  EXTERN  extern
#include "vars.h"

#define   QZERO  1.e-6  /* Equivalent to zero flow */
#define   CBIG   1.e8   /* Big coefficient         */
#define   CSMALL 1.e-6  /* Small coefficient       */


void ep_valvecoeff(int k)
/*
**--------------------------------------------------------------
**   Input:   k    = link index                                   
**   Output:  none                                                
**   Purpose: computes solution matrix coeffs. for a completely
**            open, closed, or throttled control valve.                                               
**--------------------------------------------------------------
*/
{
   double p;

   // Valve is closed. Use a very small matrix coeff.
   if (LinkStatus[k] <= CLOSED)
   {
      P[k] = 1.0/CBIG;
      Y[k] = Q[k];
      return;
   }

   // Account for any minor headloss through the valve
   if (Link[k].Km > 0.0)
   {
      p = 2.0*Link[k].Km*fabs(Q[k]);
      if ( p < RQtol ) p = RQtol;
      P[k] = 1.0/p;
      Y[k] = Q[k]/2.0;
   }
   else
   {
      P[k] = 1.0/RQtol;
      Y[k] = Q[k];
   }
}


void  ep_pipecoeff(int k)
/*
**--------------------------------------------------------------
**   Input:   k = link index                                      
**   Output:  none                                                
**  Purpose:  computes P & Y coefficients for pipe k              
**                                                              
**    P = inverse head loss gradient = 1/(dh/dQ)                
**    Y = flow correction term = h*P                            
**--------------------------------------------------------------
*/
{
   double  hpipe,     /* Normal head loss          */
         hml,       /* Minor head loss           */
         ml,        /* Minor loss coeff.         */
         p,         /* q*(dh/dq)                 */
         q,         /* Abs. value of flow        */
         r,         /* Resistance coeff.         */
         r1,        /* Total resistance factor   */
         f,         /* D-W friction factor       */
         dfdq;      /* Derivative of fric. fact. */

   /* For closed pipe use headloss formula: h = CBIG*q */
   if (LinkStatus[k] <= CLOSED)
   {
      P[k] = 1.0/CBIG;
      Y[k] = Q[k];
      return;
   }

   /* Evaluate headloss coefficients */
   q = ABS(Q[k]);                         /* Absolute flow       */
   ml = Link[k].Km;                       /* Minor loss coeff.   */
   r = Link[k].R;                         /* Resistance coeff.   */
   f = 1.0;                               /* D-W friction factor */
   if (Formflag == DW) f = DWcoeff(k,&dfdq);   
   r1 = f*r+ml;
 
   /* Use large P coefficient for small flow resistance product */
   if (r1*q < RQtol)
   {
      P[k] = 1.0/RQtol;
      Y[k] = Q[k]/Hexp;
      return;
   }

   /* Compute P and Y coefficients */
   if (Formflag == DW)                  /* D-W eqn. */
   {
      hpipe = r1*SQR(q);                /* Total head loss */
      p = 2.0*r1*q;                     /* |dh/dQ| */
     /* + dfdq*r*q*q;*/                 /* Ignore df/dQ term */
      p = 1.0/p;
      P[k] = p;
      Y[k] = SGN(Q[k])*hpipe*p;
   }
   else                                 /* H-W or C-M eqn.   */
   {
      hpipe = r*pow(q,Hexp);            /* Friction head loss  */
      p = Hexp*hpipe;                   /* Q*dh(friction)/dQ   */
      if (ml > 0.0)
      {
         hml = ml*q*q;                  /* Minor head loss   */
         p += 2.0*hml;                  /* Q*dh(Total)/dQ    */
      }
      else  hml = 0.0;
      p = Q[k]/p;                       /* 1 / (dh/dQ) */
      P[k] = ABS(p);
      Y[k] = p*(hpipe + hml);
   }
}                        /* End of pipecoeff */


void  ep_pumpcoeff(int k)
/*
**--------------------------------------------------------------
**   Input:   k = link index                                      
**   Output:  none                                                
**   Purpose: computes P & Y coeffs. for pump in link k           
**--------------------------------------------------------------
*/
{
   int   p;         /* Pump index             */
   double h0,       /* Shutoff head           */
         q,         /* Abs. value of flow     */
         r,         /* Flow resistance coeff. */
         n;         /* Flow exponent coeff.   */

   double setting = LinkSetting[k];
  
   /* Use high resistance pipe if pump closed or cannot deliver head */
   if (LinkStatus[k] <= CLOSED || setting == 0.0)
   {
      //pipecoeff(k);                                                          //(2.00.11 - LR)
      P[k] = 1.0/CBIG;                                                         //(2.00.11 - LR)
      Y[k] = Q[k];                                                             //(2.00.11 - LR)
      return;
   }

   q = ABS(Q[k]);
   q = MAX(q,TINY);
   p = PUMPINDEX(k);

   /* Get pump curve coefficients for custom pump curve. */
   if (Pump[p].Ptype == CUSTOM)
   {
      /* Find intercept (h0) & slope (r) of pump curve    */
      /* line segment which contains speed-adjusted flow. */
      curvecoeff(Pump[p].Hcurve, q/setting, &h0, &r);

      /* Determine head loss coefficients. */
      Pump[p].H0 = -h0;
      Pump[p].R  = -r;
      Pump[p].N  = 1.0;
   }

   /* Adjust head loss coefficients for pump speed. */
   h0 = SQR(setting)*Pump[p].H0;
   n  = Pump[p].N;
   r  = Pump[p].R*pow(setting,2.0-n);
   if (n != 1.0) r = n*r*pow(q,n-1.0);

   /* Compute inverse headloss gradient (P) and flow correction factor (Y) */
   P[k] = 1.0/MAX(r,RQtol);
   Y[k] = Q[k]/n + P[k]*h0;
}                        /* End of pumpcoeff */


void  ep_gpvcoeff(int k)
/*
**--------------------------------------------------------------
**   Input:   k = link index
**   Output:  none                                                
**   Purpose: computes P & Y coeffs. for general purpose valve   
**--------------------------------------------------------------
*/
{
   double h0,        /* Headloss curve intercept */
          q,         /* Abs. value of flow       */
          r;         /* Flow resistance coeff.   */

/*** Updated 9/7/00 ***/
   /* Treat as a pipe if valve closed */
   if (LinkStatus[k] == CLOSED) ep_valvecoeff(k); //pipecoeff(k);                          //(2.00.11 - LR)

   /* Otherwise utilize headloss curve   */
   /* whose index is stored in K */
   else
   {
      /* Find slope & intercept of headloss curve. */
      q = ABS(Q[k]);
      q = MAX(q,TINY);

/*** Updated 10/25/00 ***/
/*** Updated 12/29/00 ***/
      curvecoeff((int)ROUND(LinkSetting[k]),q,&h0,&r);

      /* Compute inverse headloss gradient (P) */
      /* and flow correction factor (Y).       */
      P[k] = 1.0 / MAX(r,RQtol);
      Y[k] = P[k]*(h0 + r*q)*SGN(Q[k]);                                        //(2.00.11 - LR)
   }
}
 

void  ep_pbvcoeff(int k)
/*
**--------------------------------------------------------------
**   Input:   k = link index                                      
**   Output:  none                                                
**   Purpose: computes P & Y coeffs. for pressure breaker valve   
**--------------------------------------------------------------
*/
{
   /* If valve fixed OPEN or CLOSED then treat as a pipe */
   if (LinkSetting[k] == MISSING || LinkSetting[k] == 0.0) ep_valvecoeff(k);  //pipecoeff(k);         //(2.00.11 - LR)

   /* If valve is active */
   else
   {
      /* Treat as a pipe if minor loss > valve setting */
      if (Link[k].Km*SQR(Q[k]) > LinkSetting[k]) ep_valvecoeff(k);  //pipecoeff(k);         //(2.00.11 - LR)

      /* Otherwise force headloss across valve to be equal to setting */
      else
      {
         P[k] = CBIG;
         Y[k] = LinkSetting[k]*CBIG;
      }
   }
}                        /* End of pbvcoeff */


void  ep_tcvcoeff(int k)
/*
**--------------------------------------------------------------
**   Input:   k = link index                                      
**   Output:  none                                                
**   Purpose: computes P & Y coeffs. for throttle control valve   
**--------------------------------------------------------------
*/
{
   double km;

   /* Save original loss coeff. for open valve */
   km = Link[k].Km;

   /* If valve not fixed OPEN or CLOSED, compute its loss coeff. */
   if (LinkSetting[k] != MISSING)
       Link[k].Km = 0.02517*LinkSetting[k]/(SQR(Link[k].Diam)*SQR(Link[k].Diam));

   /* Then apply usual pipe formulas */
   ep_valvecoeff(k);  //pipecoeff(k);                                             //(2.00.11 - LR)

   /* Restore original loss coeff. */
   Link[k].Km = km;
}                        /* End of tcvcoeff */


void fcvresid(int k, double *e_res, double *q_res)
{
   if (LinkStatus[k]==ACTIVE) *q_res = fabs(LinkSetting[k]-Q[k]);
   else *q_res=0;
   *e_res=0;
}


void prvresid(int k, double *e_res, double *q_res)
{
   double hset;
   int n2=Link[k].N2;
   hset = Node[n2].El + LinkSetting[k];
   if (LinkStatus[k]==ACTIVE) *e_res = fabs(hset-NodeHead[n2]);
   else *e_res=0;
   *q_res=0;
}


void psvresid(int k, double *e_res, double *q_res)
{
   double hset;
   int n1=Link[k].N1;
   hset = Node[n1].El + LinkSetting[k];
   if (LinkStatus[k]==ACTIVE) *e_res = fabs(hset-NodeHead[n1]);
   else *e_res=0;
   *q_res=0;
}


void valveresid(int k, double *e_res, double *q_res)
{
   switch (Link[k].Type)
   {
      case FCV:   fcvresid(k, e_res, q_res); break;
      case PRV:   prvresid(k, e_res, q_res); break;
      case PSV:   psvresid(k, e_res, q_res); break;
   }
}


void compute_residuals(double *e_res, double *q_res)
/* Calcula los residuos */
{
   int k, i, n1, n2, statChange;
   double h, 
      er,        /* Energy residual for current element */
      qr,        /* Flow residual for current element */
      *qnode;    /* Node imbalances */
   char *Saux;
      
   *e_res=0;
   *q_res=0;
   
   qnode = (double *) malloc((Nnodes+1)*sizeof(double));
   Saux = (char *) malloc((Nlinks+1)*sizeof(char));
   
   for (i=1; i<=Njuncs; i++)
      qnode[i] = -NodeDemand[i];
      
   /* Check if link states are consistent */
   for (k=1; k<=Nlinks; k++) Saux[k] = LinkStatus[k];
   statChange = FALSE;
   if (valvestatus()) statChange = TRUE;
   if (linkstatus())  statChange = TRUE;
   if (pswitch())     statChange = TRUE;
   if (statChange) {
      writelinef("State inconconsistency");
      for (k=1; k<=Nlinks; k++) LinkStatus[k] = Saux[k];
   }

   /* Examine each link of network */
   for (k=1; k<=Nlinks; k++)
   {
      n1 = Link[k].N1;           /* Start node of link */
      n2 = Link[k].N2;           /* End node of link   */

      qnode[n1] -= Q[k];
      qnode[n2] += Q[k];
      
      if (LinkStatus[k]<=CLOSED) {
         
         qr = fabs(Q[k]);
         if (qr>*q_res) *q_res=qr;
      
      } else {
               
         /* Compute P[k] = 1 / (dh/dQ) and Y[k] = h * P[k]   */
         /* for each link k (where h = link head loss).      */
         /* FCVs, PRVs, and PSVs with non-fixed status       */
         /* are analyzed later.                              */

         switch (Link[k].Type)
         {
            case CV:
            case PIPE:  ep_pipecoeff(k); break;
            case PUMP:  ep_pumpcoeff(k); break;
            case PBV:   ep_pbvcoeff(k);  break;
            case TCV:   ep_tcvcoeff(k);  break;
            case GPV:   ep_gpvcoeff(k);  break;
            case FCV:   
            case PRV:
            case PSV:
                        /* If valve not active then treat as pipe */
                        if (LinkStatus[k] != ACTIVE) ep_valvecoeff(k);  //pipecoeff(k);      //(2.00.11 - LR)    
                        else {
                           valveresid(k, &er, &qr);
                           if (er>*e_res) *e_res=er;
                           if (qr>*q_res) *q_res=qr;
                           continue;
                        }
                           
                        break;
            default:    continue;                  
         }
         
         h = Y[k]/P[k];
         er = fabs(h-NodeHead[n1]+NodeHead[n2]);
         if (er>*e_res) *e_res = er;
      }
   }
   
   for (i=1; i<=Njuncs; i++) {
      qr = fabs(qnode[i]);
      if (qr>*q_res) *q_res=qr;
   }
   
   free(qnode);
   free(Saux);
}
