/*
 **********************************************************************

 LOOPNET -- Hydraulic & Water Quality Simulator for Water Distribution Networks
 See LICENSE and AUTHORS files

 LOOPS.CPP -- Loop definition and related tasks for LOOPNET

 This module:
 1. defines a set of independent loops for the network and
    determines the matrix structure based on that set.
 2. updates the linear system and solves it (by calling function
    linsolve() from module SMATRIX.C)
 3. Computes flow correction of a link, taking into account the loops
    it belongs to
 4. Contains functions that write info about the computed results

 **********************************************************************
*/

#include <vector>
#include <deque>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "hash.h"
#include "text.h"
#include "types.h"
//#include "funcs.h"
#define  EXTERN  extern "C"
#include "vars.h"
#include "config.h"
#include <omp.h>
#include <assert.h>

extern const char *RptFormTxt[], *StatTxt[];

/* Functions defined elsewhere */
extern "C" int  linsolve(int n, double *Aii, double *Aij, const int *XLNZ,
         const int *NZSUB, double *B);
extern "C" int  linsolve_mult(int n, int nbcol, double *Aii, double *Aij, const int *XLNZ,
         const int *NZSUB, double *B);

/* Type definitions  */
typedef std::vector<std::vector<int> > TListOfLists;

/* Private functions */
static void print_lists(TListOfLists &lists);
template <typename T> static void store_listoflists(const std::vector<std::vector<T> > &ll, int **starts, T **values);
template <typename T> static void store_listoflists_reordered(const std::vector< std::vector<T> > &ll, int **starts, T **values, const int order[]);

/*******************************************************************************
 * Data type for loop adjacencies
 ******************************************************************************/

/* Adjacency/overlap between two loops */
struct SAdjElem{
   int loop;
   std::vector<int> *plinks;        /* List of links in common to the two loops */
   struct SAdjElem *next;
};

typedef struct SAdjElem TAdjElem;

typedef struct {
   int n;
   int ncoeffs;
   TAdjElem **adjlist;
   int *linkcount;
} TAdj;


static void adj_init(TAdj *adj, int n, int nlinks)
{
   adj->n = n;
   adj->ncoeffs=0;
   adj->adjlist = (TAdjElem **) calloc(n+1, sizeof(TAdjElem *));
   assert(adj->adjlist!=NULL);
   adj->linkcount = (int *) calloc(nlinks+1, sizeof(int *));
   assert(adj->linkcount!=NULL);
}


static void  adj_free(TAdj *adj)
/*
**--------------------------------------------------------------
** Input:   none
** Output:  none
** Purpose: frees memory used for loop adjacency lists
**--------------------------------------------------------------
*/
{
   int   i;
   TAdjElem *adjel;

   for (i=0; i<=adj->n; i++)
   {
      for (adjel = adj->adjlist[i]; adjel != NULL; adjel = adj->adjlist[i])
      {
         adj->adjlist[i] = adjel->next;
         if (adjel->loop > i) delete adjel->plinks;
         free(adjel);
      }
   }
   free(adj->adjlist);
}                        /* End of freelists */


static int  adj_addadj_simple(TAdj *adj, int i, int j, std::vector<int> *plinks)
/*
**--------------------------------------------------------------
** Input:   i = loop index
**          j = loop index
**          plinks: pointer to a vector of links common to loops i and j
** Output:  returns 1
** Purpose: augments loop i's adjacency list with loop j
**--------------------------------------------------------------
*/
{
   TAdjElem *adjel;
   adjel = (TAdjElem *) malloc(sizeof(TAdjElem));
   assert(adjel!=NULL);
   adjel->loop = j;
   adjel->plinks = plinks;
   adjel->next = adj->adjlist[i];
   adj->adjlist[i] = adjel;
   return(1);
}                        /* End of addlink */


static TAdjElem *adj_adjacent(const TAdj *adj, int i, int j)
/*
**--------------------------------------------------------------
** Input:   i = loop index
**          j = loop index
** Output:  If loops i and j are adjacent, a pointer to the adjacency is returned
**          Otherwise, NULL is returned
** Purpose: checks if loops i and j are adjacent.
**--------------------------------------------------------------
*/
{
   TAdjElem *adjel;
   for (adjel = adj->adjlist[i]; adjel != NULL; adjel = adjel->next)
      if (adjel->loop == j) return adjel;
   return NULL;
}                        /* End of linked */


static int  adj_addadj(TAdj *adj, int i, int j, int link, int symm)
/*
**--------------------------------------------------------------
** Input:   i = loop index
**          j = loop index
**          link = link index
**          symm = indicates if the adjacency is symmetric
** Output:  returns 0 if loops were already adjacent, 1 otherwise
** Purpose: Adds adjacency between loops i and j. It first checks if the loops
**          are already adjacent
**--------------------------------------------------------------
*/
{
   TAdjElem *adjel;

   if ((adjel=adj_adjacent(adj, i, j)) != NULL) {
      /* Loops are already adjacent. Add the link to the list of common links */
      if (link!=0) adjel->plinks->push_back(link);
      adj->linkcount[ABS(link)]++;
      return 0;
   }
   else {
      /* Loops are not adjacent yet. Add new adjacency */
      std::vector<int> *plinks=NULL;
      if (link!=0) {
         plinks = new std::vector<int>();
         plinks->push_back(link);
         adj->linkcount[ABS(link)]++;
      }
      adj_addadj_simple(adj, i, j, plinks);
      if (symm) adj_addadj_simple(adj, j, i, plinks);
      adj->ncoeffs++;
      return 1;
   }
}                        /* End of adj_addadj */



static void adj_sortlist(TAdjElem **start, const int row[])
/*
 * sort a list of adjacencies by the row numbers of the corresponding loops
 */
{
   TAdjElem **padj1, **padj2, **padjmin, *adjmin;

   for (padj1=start; *padj1!=NULL; padj1=&((*padj1)->next)) {
      /* Find loop with minimum row number */
      padjmin=padj1;
      for (padj2=padj1; *padj2!=NULL; padj2=&((*padj2)->next)) {
         if (row[(*padj2)->loop] < row[(*padjmin)->loop])
            padjmin = padj2;
      }
      /* Bring loop forward, following *padj1 */
      if (padjmin!=padj1) {
         /* remove *padjmin from its current position */
         adjmin = *padjmin;
         *padjmin = adjmin->next;
         /* insert it in its new position */
         adjmin->next = *padj1;
         *padj1 = adjmin;
      }
   }
}


static void adj_print(const TAdj *adj)
{
   printf("%d listas de adyacencias\n", adj->n);
   printf("%d adyacencias\n", adj->ncoeffs);
   printf("%d elementos no nulos\n", adj->ncoeffs+adj->n);
   TAdjElem *adjel;

   for (int i=1; i<=adj->n; i++) {
      printf ("%d. [ ", i);
      for (adjel=adj->adjlist[i]; adjel!=NULL; adjel=adjel->next)
         printf("%d ", adjel->loop);
      printf("]\n");
   }
}


/*-------------- Functions for matrix reordering by minimum degree ----------*/


void  countdegree(const TAdj *adj, int *degree)
/*
**----------------------------------------------------------------
** Input:   none
** Output:  none
** Purpose: counts number of loops adjacent to each loop
**----------------------------------------------------------------
*/
{
    int   i;
    TAdjElem *adjel;
    memset(degree,0,(adj->n+1)*sizeof(int));

    for (i=1; i<=adj->n; i++)
        for (adjel = adj->adjlist[i]; adjel != NULL; adjel = adjel->next)
            degree[i]++;
}



static int  mindegree(const TAdj *adj, int k, const int *order, const int *degree)
/*
**--------------------------------------------------------------
** Input:   k = first loop in list of active loops
** Output:  returns loop index with fewest adjacencies
** Purpose: finds active loop with fewest adjacencies
**--------------------------------------------------------------
*/
{
   int i, m;
   int n=adj->n;
   int min = n,
       imin = n;

   for (i=k; i<=n; i++)
   {
      m = degree[order[i]];
      if (m < min)
      {
         min = m;
         imin = i;
      }
   }
   return(imin);
}                        /* End of mindegree */


static int  newlink(TAdj *adj, TAdjElem *adjel, int *degree)
/*
**--------------------------------------------------------------
** Input:   adjel = element of loop's adjacency list
** Output:  returns 1 if successful, 0 if not
** Purpose: links end of adjacency element adjel to end loops of
**          all elements that follow it on adjacency list
**--------------------------------------------------------------
*/
{
   int   iloop, jloop;
   TAdjElem *adjel2;

   /* Scan all entries in adjacency list that follow adjel. */
   iloop = adjel->loop;             /* End loop of connection element adjel */
   for (adjel2 = adjel->next; adjel2 != NULL; adjel2 = adjel2->next)
   {
      jloop = adjel2->loop;          /* End loop of next connection */

      /* If jloop still active, and iloop not connected to jloop, */
      /* then add a new connection between iloop and jloop.       */
      if (degree[jloop] > 0)        /* jloop still active */
      {
         /* Update adjacency lists for iloop & jloop to reflect the connection. */
         if (adj_addadj(adj,iloop,jloop,0,1))
         {
            //printf("Adding non-zero coeff between loops %d and %d\n", iloop, jloop);
            degree[iloop]++;
            degree[jloop]++;
         }
      }
   }
   return(1);
}                        /* End of newlink */


static int  growlist(TAdj *adj, int kloop, int *degree)
/*
**--------------------------------------------------------------
** Input:   kloop = loop index
** Output:  returns 1 if successful, 0 if not
** Purpose: creates new entries in kloop's adjacency list for
**          all unlinked pairs of active loops that are
**          adjacent to kloop
**--------------------------------------------------------------
*/
{
   int   loop;
   TAdjElem *adjel;

   /* Iterate through all loops connected to kloop */
   for (adjel = adj->adjlist[kloop]; adjel != NULL; adjel = adjel -> next)
   {
      loop = adjel->loop;       /* End loop of adjacency  */
      if (degree[loop] > 0)     /* End loop is active           */
      {
         degree[loop]--;        /* Reduce degree of adjacency   */
         if (!newlink(adj,adjel,degree))   /* Add to adjacency list        */
            return(0);
      }
   }
   return(1);
}                        /* End of growlist */


static int   reorderloops(TAdj *adj, int row[], int order[])
/*
**--------------------------------------------------------------
** Output:  returns 1 if successful, 0 if not
** Purpose: re-orders loops to minimize # of non-zeros that
**          will appear in factorized solution matrix
**--------------------------------------------------------------
*/
{
   int k, kloop, m,
      n=adj->n;

   int *degree = (int *) calloc(n+1, sizeof(int));
   assert(degree!=NULL);
   countdegree(adj, degree);

   for (k=0; k<=n; k++)
   {
      row[k] = k;
      order[k] = k;
   }
   for (k=1; k<=n; k++)                       /* Examine each loop    */
   {
      m = mindegree(adj,k,order,degree);      /* Loop with lowest degree  */
      kloop = order[m];                       /* Loop's index             */
      //printf("Selecting loop %d for row %d\n", kloop, k);
      if (!growlist(adj,kloop,degree))        /* Augment adjacency list   */
         return(101);
      order[m] = order[k];                    /* Switch order of loops    */
      order[k] = kloop;
      degree[kloop] = 0;                      /* In-activate loop         */
   }
   for (k=1; k<=n; k++)                       /* Assign loops to rows of  */
     row[order[k]] = k;                       /*   coeff. matrix          */

   free(degree);
   return(0);
}                        /* End of reorderloops */

/*------------------------------------------------------------------------*/

/* Automatic Valves (PRV and PSV) */
typedef struct {
   int nautov;          /* Num of automatic valves */
   int *avlink;         /* Link index corresponding to each automatic valve index 
                           (indexed from 1 to nautov) */
   int *linkav;         /* Automatic valve index corresponding to each link index */
} TAutoValves;

static void autov_init(TAutoValves *autovlv)
{
   int i, k;

   /* Get PRV info */
   autovlv->avlink = (int *) malloc((Nvalves+1)*sizeof(int));
   autovlv->linkav = (int *) calloc((Nlinks+1),sizeof(int));
   autovlv->nautov=0;
   for (i=1; i<=Nvalves; i++) {
      k = Valve[i].Link;
      if (Link[k].Type==PRV || Link[k].Type==PSV) {
         autovlv->nautov++;
         autovlv->avlink[autovlv->nautov] = k;
         autovlv->linkav[k]=autovlv->nautov;
      }
   }
}

static void autov_free(TAutoValves *autovlv)
{
   free(autovlv->avlink);
   free(autovlv->linkav);
}

static int autov_get_link_control_node(int ilink)
/* Get control node of a PRV/PSV, given its link number */
{
   assert(Link[ilink].Type==PRV || Link[ilink].Type==PSV);
   if (Link[ilink].Type==PRV)
      return Link[ilink].N2;
   else
      return Link[ilink].N1;
}

static int autov_get_link_non_control_node(int ilink)
/* Get non-control node of a PRV/PSV, given its link number */
{
   assert(Link[ilink].Type==PRV || Link[ilink].Type==PSV);
   if (Link[ilink].Type==PRV)
      return Link[ilink].N1;
   else
      return Link[ilink].N2;
}

/*-------------- Data type for matrix storage ----------*/

typedef struct {
   double *A;
   int m, n;
   int lda;
   int external_buffer;
} TDenseMat;

static void dmat_init(TDenseMat *mat, int m, int n)
{
   mat->m = m;
   mat->n = n;
   mat->lda = n;
   mat->external_buffer = 0;
   mat->A = (double *) malloc(m*n*sizeof(double));
   assert(mat->A!=NULL);
}

static void dmat_attach_buffer(TDenseMat *mat, int m, int n, int lda, double *buf)
{
   mat->m = m;
   mat->n = n;
   mat->lda = lda;
   mat->external_buffer = 1;
   mat->A = buf;
}

static void dmat_free(TDenseMat *mat)
{
   if (!mat->external_buffer) free(mat->A);
}

static void dmat_print(const TDenseMat *mat)
{
   int i, j, m=mat->m, n=mat->n, lda=mat->lda;
   double *Adense=mat->A;

   for (i=0; i<m; i++) {
      for (j=0; j<n; j++)
         printf("%10g  ", Adense[i*lda+j]);
      printf("\n");
   }
}

static void mydgemm(int m, int n, int k, const double A[], int lda,
         const double B[], int ldb, double C[], int ldc)
/* C := -C + A*B
 * where C is m x n, A is m x k, B is k x n */
{
   int i, j, kk;
   double x;
   double *c_row;
   const double *a_row, *b_col;

   for (i=0; i<m; i++) {
      c_row=&C[i*ldc];
      for (j=0; j<n; j++) {
         x=-c_row[j];
         a_row=&A[i*lda];
         b_col=&B[j];
         for (kk=0; kk<k; kk++)
            x += a_row[kk]*b_col[kk*ldb];
         c_row[j]=x;
      }
   }
}

static void mydgemv(int m, int n, const double A[], int lda,
         const double x[], int incx, double y[], int incy)
/* Operacion y := y - A*x */
{
   int i, j;
   double aux;
   const double *Ai;

   for (i=0; i<m; i++) {
      Ai = &A[i*lda];
      aux = y[i*incy];
      for (j=0; j<n; j++)
         aux -= Ai[j]*x[j*incx];
      y[i*incy] = aux;
   }
}

static void mydgetrf(int n, double A[], int lda, int perm[])
{
   int i, j, k;
   double *Ak, *Ai, *A_col_k, mult, aux, maxelem;
   int perm_pr,                          /* Position on perm of the pivot row */
       pr;                               /* Pivot row */
   int perm_done=0;

   for (i=0; i<n; i++) perm[i]=i;

   for (k=0; k<n-1; k++) {

      /* Find pivot row */
      A_col_k = &A[k];      /* Column k */
      maxelem = fabs(A_col_k[perm[k]*lda]);
      perm_pr = k;
      for (i=k+1; i<n; i++) {
         aux = fabs(A_col_k[perm[i]*lda]);
         if (aux>maxelem) {
            maxelem=aux;
            perm_pr = i;
         }
      }
      /* Update permutation vector (swap elements k and perm_pr) */
      if (perm_pr!=k) {
         perm_done=1;
         pr = perm[perm_pr];
         perm[perm_pr] = perm[k];
         perm[k] = pr;
      }
      else
         pr = perm[k];

      /* Update rows below the pivot row */
      Ak=&A[pr*lda];         /* Pivot row */
      for (i=k+1; i<n; i++) {
         Ai=&A[perm[i]*lda];
         mult=Ai[k]/Ak[k];
         Ai[k]=mult;
         for (j=k+1; j<n; j++)
            Ai[j] -= mult*Ak[j];
      }
   }
   if (perm_done)
      printf("\nSe ha hecho permutacion\n");
}

static void mydltrsv( int n, const double A[], int lda, double x[], int incx,
         const int perm[])
{
   const double *Ai;
   double xi;
   int i, j;
   for (i=0; i<n; i++) {
      Ai = &A[perm[i]*lda];
      xi = x[perm[i]*incx];
      for (j=0; j<i; j++)
         xi -= Ai[j]*x[perm[j]*incx];
      x[perm[i]*incx] = xi;
   }
}

static void mydutrsv( int n, const double A[], int lda, double x[], int incx,
         const int perm[])
{
   const double *Ai;
   double xi;
   int i, j;
   for (i=n-1; i>=0; i--) {
      Ai = &A[perm[i]*lda];
      xi = x[perm[i]*incx];
      for (j=i+1; j<n; j++)
         xi -= Ai[j]*x[perm[j]*incx];
      x[perm[i]*incx] = xi/Ai[i];
   }
}

static void mydgesv(int n, double A[], int lda, double x[], int incx)
{
   int *perm = (int *) malloc(n*sizeof(int));     /* Permutation vector */
   assert(perm!=NULL);

   //{
      //TDenseMat mat;
      //printf("\nMatriz PRV antes de GESV:\n");
      //dmat_attach_buffer(&mat, n, n, lda, A);
      //dmat_print(&mat);
   //}

   mydgetrf(n, A, lda, perm);
   mydltrsv(n, A, lda, x, incx, perm);
   mydutrsv(n, A, lda, x, incx, perm);

   //{
      //TDenseMat mat;
      //printf("\nVector de permutaciones:\n");
      //for (int i=0; i<n; i++)
         //printf("%d\n", perm[i]);
      //printf("\nMatriz PRV despues de GESV:\n");
      //dmat_attach_buffer(&mat, n, n, lda, A);
      //dmat_print(&mat);
   //}

   free(perm);
}

#define A21(i,j) A21[(i-1)*nloops+j-1]
#define Ab(i,j)  Ab[(i-1)*(nautov+1)+j-1]
#define mat_b(mat,i) (mat)->b[(i-1)*((mat)->nautov+1)]
#define b2(i)    b2[(i-1)*(nautov+1)]

typedef struct {
   /* The system is composed of the matrix A and the RHS b, partitioned in the following way:
    *   A = [ A11 A12 ]   b = [ b1 ]
    *       [ A21 A22 ]       [ b2 ]
    * where:
    * A11: headloss derivatives of loops. Sparse symmetric positive definite matrix, with
    *    nloops rows and cols. This is stored in CSC format.
    * A21: headloss derivative of links in the PRV paths. Matrix with nautov rows, nloops cols
    * A12: valves involved in each loop. Matrix with nloops rows, nautov cols, elem i,j is:
    *   +1 if the PRV j is in loop i, with positive direction,
    *   -1 if the PRV j is in loop i, with negative direction,
    *    0 if the PRV j is not y loop i
    * A22: valves involved in the PRV paths. Matrix with nautov rows, nautov cols, elem i,j is:
    *   +1 if the PRV j is in the path of PRV i, with positive direction,
    *   -1 if the PRV j is in the path of PRV i, with negative direction,
    *    0 if the PRV j is in the path of PRV i
    */
   int nloops,
       ncoeffs,         /* Non-zero coeffs of A11, excluding the diagonal */
       buflen;
   int *xlnz;           /* start index of each column of A11 */
   int *nzsub;          /* row indices of each non-zero coeff of A11 */
   std::vector<int> **lnz;      /* links for each non-zero coeff of A11 */
   double *Aii, *Aij;   /* Non-zero coeffs of A11 */
   int nautov;
   double   *A21,
            *Ab,   /* Matrix [ A12 b1 ]
                    *        [ A22 b2 ] */
            *b,
            *b2;
} TSystemMatrix;


void mat_init(TSystemMatrix *mat, int nloops, int ncoeffs, int nautov)
{
   mat->nloops = nloops;
   mat->ncoeffs = ncoeffs;
   mat->nautov = nautov;
   mat->xlnz  = (int *) calloc(nloops+2, sizeof(int));
   assert(mat->xlnz!=NULL);
   mat->nzsub = (int *) calloc(ncoeffs+2, sizeof(int));
   assert(mat->nzsub!=NULL);
   mat->lnz = (std::vector<int> **) calloc(ncoeffs+2, sizeof(void *));
   assert(mat->lnz!=NULL);
   mat->Aii = NULL;   /* Memory for non-zero elements not allocated */
}

void mat_allocnz(TSystemMatrix *mat)
/* Allocate memory for non-zero elemens */
{
   int nloops=mat->nloops, ncoeffs=mat->ncoeffs, nautov=mat->nautov;
      
   mat->buflen = nloops+1+                    /* Aii */
                 ncoeffs+1+                   /* Aij */
                 nautov*nloops+               /* A21 */
                 (nloops+nautov)*(nautov+1);  /* Ab */
   mat->Aii = (double *) malloc(mat->buflen*sizeof(double));
   assert(mat->Aii!=NULL);
   mat->Aij = &mat->Aii[nloops+1];
   mat->A21 = &mat->Aij[ncoeffs+1];
   mat->Ab  = &mat->A21[nautov*nloops];
   mat->b  = &mat->Ab[nautov];
   mat->b2 = &mat->b[nloops*(nautov+1)];
}

void mat_free_lnz(TSystemMatrix *mat)
{
   free(mat->lnz);
   mat->lnz=NULL;
}

void mat_free(TSystemMatrix *mat)
{
   free(mat->xlnz);
   free(mat->nzsub);
   if (mat->lnz!=NULL) free(mat->lnz);
   if (mat->Aii!=NULL) free(mat->Aii);
}

static double mat_elem(const TSystemMatrix *mat, int i, int j)
{
   double x=0;
   int nloops=mat->nloops, nautov=mat->nautov;
   int aux, k;
   if (i<=nloops && j<=nloops) {
      if (i==j) x=mat->Aii[i];
      else {
         if (j>i) {
            /* swap i, j */
            aux=j;
            j=i;
            i=aux;
         }
         for (k=mat->xlnz[j]; k<mat->xlnz[j+1]; k++) {
            if (mat->nzsub[k]==i) {
               x=mat->Aij[k];
               break;
            }
         }
      }
   }
   else if (j>nloops)
      x = mat->Ab(i,j-nloops);
   else
      x = mat->A21(i-nloops,j);

   return x;
}

static void mat_print_A11(const TSystemMatrix *mat)
{
   int i,j,k;
   for (i=1; i<=mat->nloops; i++) {
      printf("%d. [ ", i);
      for (k=mat->xlnz[i]; k<mat->xlnz[i+1]; k++) {
         j=mat->nzsub[k];
         printf("%d, ", j);
      }
      printf("]\n");
   }
}

static void mat_to_dense(TSystemMatrix *mat, TDenseMat *dmat)
{
   int i, j, k;
   int nloops=mat->nloops;
   int nautov=mat->nautov;
   int m=nloops+nautov, n=m+1;

   dmat_init(dmat, m, n);
   double *Adense = dmat->A;

   for (i=0; i<nloops; i++)
      for (j=0; j<nloops; j++)
         Adense[i*n+j]=0;

   for (j=1; j<=nloops; j++) {
      Adense[(j-1)*n+(j-1)] = mat->Aii[j];
      for (k=mat->xlnz[j]; k<mat->xlnz[j+1]; k++) {
         i = mat->nzsub[k];
         Adense[(i-1)*n+(j-1)] = mat->Aij[k];
         Adense[(j-1)*n+(i-1)] = mat->Aij[k];
      }
   }

   /* [ A12 b1 ] */
   for (i=0; i<nloops; i++)
      for (j=0; j<nautov+1; j++)
         Adense[i*n+(nloops+j)] = mat->Ab(i+1,j+1);

   /* A21 */
   for (i=0; i<nautov; i++)
      for (j=0; j<nloops; j++)
         Adense[(nloops+i)*n+j] = mat->A21(i+1,j+1);

   /* [ A22 b2 ] */
   for (i=nloops; i<nloops+nautov; i++)
      for (j=0; j<nautov+1; j++)
         Adense[i*n+(nloops+j)] = mat->Ab(i+1,j+1);
}

static void linsys_print(const TSystemMatrix *mat)
/* Print linear system */
{
   int i, j;
   int nloops=mat->nloops, nautov=mat->nautov, n=nloops+nautov;
   for (i=1; i<=n; i++) {
      for (j=1; j<=n+1; j++)
         printf("%24.16e ", mat_elem(mat, i,j));
      printf("\n");
   }
}

/*-------------- Functions for storage of link positions in matrix ----------*/

/* Info connecting links with corresponding system coeffs */
typedef struct {
   /* Adjacency info */
   int adj_n;
   int *adj_st, *adj_links;        /* Links for each adjacency */
   double **adj_pos;               /* Position of non-zero for each adjacency */

   /* Positions of coeffs to be set to zero for each inactive PRV/PSV */
   int *prvcoeff_st;               /* Start for each PRV in array prvcoeff_pos */
   double **prvcoeff_pos;          /* Positions of PRV coeffs */

   /* Row index for each PRV path */
   int *prvrow;
} TLinkPos;


static void lpos_init(TLinkPos *lpos, std::vector<double*> &vadj_pos, std::vector<int> &vadj_st,
      std::vector<int> &vadj_links, std::vector<std::vector<double *> > &prvloop_coeffs,
      const TSystemMatrix *mat, const int row[])
/* Initialize lpos structure */
{
   int iupd, i;
   int nautov = (int) prvloop_coeffs.size()-1;
   int nloops_noprv = mat->nloops + mat->nautov - nautov;
   int adj_n, *adj_st, *adj_links;
   double **adj_pos;
   int *prvrow;

   /* Convert vadj_* vectors to plain arrays */
   adj_n = lpos->adj_n = (int) vadj_pos.size()-1;
   iupd = (int) vadj_links.size();
   adj_st    = lpos->adj_st    = (int *) malloc((adj_n+2)*sizeof(int));
   adj_pos   = lpos->adj_pos   = (double **) malloc((adj_n+1)*sizeof(double *));
   adj_links = lpos->adj_links = (int *) malloc(iupd*sizeof(int));
   for (i=1; i<=adj_n+1; i++) {
      adj_st[i] = vadj_st[i];
      //printf("Adyacencia %d empieza en posicion %d\n", i, adj_st[i]);
   }
   for (i=1; i<=adj_n; i++)   adj_pos[i] = vadj_pos[i];
   for (i=0; i<iupd; i++) {
      adj_links[i] = vadj_links[i];
      //printf("Actualizacion %d: linea %d\n", i, s_adj_links[i]);
   }

   /* Copy prvloop_coeffs, converting to plain arrays */
   store_listoflists(prvloop_coeffs, &(lpos->prvcoeff_st), &(lpos->prvcoeff_pos));

   /* Set rows corresponding to PRV/PSV */
   prvrow = lpos->prvrow = (int *) malloc((nautov+1)*sizeof(int));
   for (i=1; i<=nautov; i++)
      prvrow[i] = row[nloops_noprv+i];
}

static void lpos_print(const TLinkPos *lpos, const TAutoValves *autovlv)
{
   int i, j;
   int nadj=lpos->adj_n;
   int nupd=lpos->adj_st[nadj+1];
   double *px=lpos->adj_pos[1];
   for (i=1; i<=nadj; i++) {
      printf("Adjacency %d: updates [%d-%d).", i, lpos->adj_st[i], lpos->adj_st[i+1]);
      printf(" Non-zero at position %ld\n", (long) (lpos->adj_pos[i]-px));
   }
   for (i=0; i<nupd; i++) {
      printf("Update %d: link %d\n", i, lpos->adj_links[i]);
   }
   if (autovlv->nautov>0) {
      printf("Coeffs to be set to zero for inactive PRV/PSV valves:\n");
      for (i=1; i<=autovlv->nautov; i++) {
         printf("Valve %d (link %d). Positions:", i, autovlv->avlink[i]);
         for (j=lpos->prvcoeff_st[i]; j<lpos->prvcoeff_st[i+1]; j++)
            printf(" %ld", (long) (lpos->prvcoeff_pos[j]-px));
         printf("\n");
      }
   }
   printf("\n");
}

static void lpos_free(TLinkPos *lpos)
{
   free(lpos->adj_st);
   free(lpos->adj_pos);
   free(lpos->adj_links);
}

static void mat_setprv_nosym(TSystemMatrix *mat, const int row[], const TListOfLists *edge_loops,
         const TListOfLists *loops, const TAutoValves *autovlv, std::vector<int> &vadj_st,
         std::vector<int> &vadj_links, std::vector<double*> &vadj_pos)
{
   int i, j, iv, k2, k2_signed, kpos, iloop, iloop_signed, iupd;
   int nautov = autovlv->nautov, nloops = Nlinks-Njuncs;
   const std::vector<int> *pvect;
   TAdj adj;
   TAdjElem *adjel;

   adj_init(&adj, nautov, Nlinks);

   for (iv=1; iv<=nautov; iv++) {

      /* Examine path from the valve iv to a tank/reservoir */
      for (i=0; i<(int)(*loops)[nloops+iv].size(); i++) {
         k2_signed = (*loops)[nloops+iv][i];
         k2 = ABS(k2_signed);                /* k2: index of a link in the path */

         if (!(Link[k2].Type==PRV || Link[k2].Type==PSV)) {
            /* Examine the loops where link k2 is present */
            pvect = &(*edge_loops)[k2];
            for (kpos=0; kpos<(int)pvect->size(); kpos++) {
               iloop_signed = (*pvect)[kpos];
               iloop = ABS(iloop_signed);

               /* k2 belongs to loops iv and iloop. Add adjacency */
               //printf("Adyacencia en A21 en fila %d, col %d, linea %d\n", iv, row[iloop],
               //      (k2_signed*iloop_signed>0)?k2:-k2);
               adj_addadj(&adj, iv, iloop, (k2_signed*iloop_signed>0)?k2:-k2, 0);
            }
         }
      }
   }

   /* Get the position and the list of links corresponding to each adjacency */
   iupd = (int) vadj_links.size();
   for (i=1; i<=nautov; i++)             /* column */
   {
      /* sort list of adjacencies for loop i, by row number */
      adj_sortlist(&(adj.adjlist[i]), row);

      for (adjel = adj.adjlist[i]; adjel != NULL; adjel = adjel->next)
      {
         j = row[adjel->loop];
         /* Copy the list of links of the adjacency*/
         for (size_t ilink=0; ilink<adjel->plinks->size(); ilink++) {
            //printf("Adyacencia en A21 en fila %d, col %d, linea %d\n", i, j, (*(adjel->plinks))[ilink]);
            vadj_links.push_back((*(adjel->plinks))[ilink]);
            iupd++;
         }
         vadj_pos.push_back(&(mat->A21(i,j)));   /* Set position of non-zero coeff */
         vadj_st.push_back(iupd);                /* Set start index for the links of next adjacency */
      }
   }

   adj_free(&adj);
}


static void mat_setprv_nosym_conns(TSystemMatrix *mat, const TLinkPos *lpos,
         const TAutoValves *autovlv, const int *looptanks)
{
   int j, k, iv, iv2, k2, k2_signed, iloop_signed, iloop, kpos;
   int nautov = autovlv->nautov;
   const int *avlink = autovlv->avlink;
   const int *linkav = autovlv->linkav;
   int nloops = mat->nloops;

   for (iv=1; iv<=nautov; iv++) {
      k = avlink[iv];

      if (LinkStatus[k]==ACTIVE) {

         /* Set A22 row for the valve */
         /* Examine path from the valve iv to a tank/reservoir */
         for (int ilink=LoopLinks_start[nloops+iv]; ilink<LoopLinks_start[nloops+iv+1]; ilink++) {
            k2_signed = LoopLinks_links[ilink];
            k2 = ABS(k2_signed);                /* k2: index of a link in the path */

            if (linkav[k2]!=0) {
               /* k2 is an automatic valve. Set A22 element */
               iv2 = linkav[k2];
               mat->Ab(nloops+iv,iv2) = (k2_signed>0)?1:-1;
            }

            /* Add link headloss to RHS vector */
            if (k2_signed>0) mat->b2(iv) -= Y[k2];
            else             mat->b2(iv) += Y[k2];
         }

         /* Add PRV/PSV setting and tank head to RHS vector */
         int control_node = autov_get_link_control_node(k);
         mat->b2(iv) += Node[control_node].El + LinkSetting[k] -
                                       NodeHead[looptanks[2*(nloops+iv)+1]];

         /* Set A12 column for the valve */
         /* Examine the loops where the valve is present */
         for (kpos=LinkLoops_start[k]; kpos<LinkLoops_start[k+1]; kpos++) {
            iloop_signed = LinkLoops_loops[kpos];
            iloop = ABS(iloop_signed);
            mat->Ab(iloop,iv) = (iloop_signed>0)? 1: -1;
         }
      }
      else {                                                    /* Inactive valve */

         /* Adjust equation of valve iv to make the headloss unknown equal to 0 */
         for (j=1; j<=nloops; j++)                /* zeros in row iv of matrix A21 */
            mat->A21(iv,j)=0;
         for (j=1; j<=nautov; j++)                  /* zeros in row iv of matrix A22 */
            mat->Ab(nloops+iv,j)=0;
         mat->Ab(nloops+iv,iv) = 1000;         /* Diagonal element */
         mat->Ab(nloops+iv,nautov+1) = 0;           /* RHS element in b2 */
      }
   }
}

static void mat_setprv_sym_conns(TSystemMatrix *mat, const TLinkPos *lpos,
         const TAutoValves *autovlv, const int *looptanks)
{
   int k, iv;
   int nautov = autovlv->nautov;
   const int *avlink = autovlv->avlink;
   const int *prvrow = lpos->prvrow;
   const int *prvcoeff_st = lpos->prvcoeff_st;
   double * const *prvcoeff_pos = lpos->prvcoeff_pos;
   int *xlnz = mat->xlnz;

   for (iv=1; iv<=nautov; iv++) {
      k = avlink[iv];
      int irow=prvrow[iv];

      if (LinkStatus[k]==ACTIVE) {
            /* Correct RHS element of loop, taking into account PRV/PSV setting */
            int control_node = autov_get_link_control_node(k);
            double hset = Node[control_node].El + LinkSetting[k];
            /* OJO: asumimos que los caminos de PRV van de la PRV al deposito y no al reves */
            mat_b(mat,irow) += hset;
      }
      else {
         /* Zero out row corresp. to PRV path */
         for (int i=prvcoeff_st[iv]; i<prvcoeff_st[iv+1]; i++)
            *(prvcoeff_pos[i]) = 0;
         /* Zero out column corresp. to PRV path */
         for (int i=xlnz[irow]; i<xlnz[irow+1]; i++)
            mat->Aij[i] = 0;
         /* Set diagonal and RHS elements so that flow correction is zero */
         mat->Aii[irow] = 1000;
         mat_b(mat,irow) = 0;
      }
   }
}

static void  transpose(int n, int *il, int *jl, std::vector<int> **xl,
               int *ilt, int *jlt, std::vector<int> **xlt, int *nzt)
/*
**---------------------------------------------------------------------
** Input:   n = matrix order
**          il,jl,xl = sparse storage scheme for original matrix
**          nzt = work array
** Output:  ilt,jlt,xlt = sparse storage scheme for transposed matrix
** Purpose: Determines sparse storage scheme for transpose of a matrix
**---------------------------------------------------------------------
*/
{
   int  i, j, k, kk;

   for (i=1; i<=n; i++) nzt[i] = 0;
   for (i=1; i<=n; i++)
   {
      for (k=il[i]; k<il[i+1]; k++)
      {
         j = jl[k];
         kk = ilt[j] + nzt[j];
         jlt[kk] = i;
         xlt[kk] = xl[k];
         nzt[j]++;
      }
   }
}                        /* End of transpose */


static void storesparse(TSystemMatrix *mat, const TAdj *adj,
      const int row[], const int order[], int nloops)
{
   TAdjElem *adjel;
   int   i, ii, j, k, m;
   int   nloops_adj = adj->n;                /* num loops with adjacency info */
   int   ncoeffs = adj->ncoeffs;
   std::vector<int> **lnz;
   int *nzsub;

   /* Allocate sparse matrix storage */
   mat_init(mat, nloops_adj, ncoeffs, nloops-nloops_adj);

   /* Generate row index pointers for each column of matrix */
   nzsub = mat->nzsub;
   lnz = mat->lnz;
   k = 0;
   mat->xlnz[1] = 1;
   for (i=1; i<=nloops_adj; i++)             /* column i */
   {
      m = 0;
      ii = order[i];
      for (adjel = adj->adjlist[ii]; adjel != NULL; adjel = adjel->next)
      {
         j = row[adjel->loop];               /* row j */
         if (j<i)
         {
            m++;
            k++;
            nzsub[k] = j;
            lnz[k] = adjel->plinks;
         }
      }
      mat->xlnz[i+1] = mat->xlnz[i] + m;
   }
}


static void ordersparse(TSystemMatrix *A)
/*
**--------------------------------------------------------------
** Input:   n = number of rows in solution matrix
** Output:  returns eror code
** Purpose: puts row indexes in ascending order in NZSUB
**--------------------------------------------------------------
*/
{
   int n = A->nloops;
   int  i, k;
   int  *xlnz=A->xlnz, *nzsub=A->nzsub;
   int  *xlnzt, *nzsubt, *nzt;
   std::vector<int> **lnz=A->lnz, **lnzt;
   TSystemMatrix _At, *At=&_At;

   mat_init(At, n, A->ncoeffs, A->nautov);
   xlnzt=At->xlnz; nzsubt=At->nzsub; lnzt=At->lnz;

   nzt    = (int *) calloc(n+2, sizeof(int));

   /* Count # non-zeros in each row */
   for (i=1; i<=n; i++) nzt[i] = 0;
   for (i=1; i<=n; i++)
   {
       for (k=xlnz[i]; k<xlnz[i+1]; k++) nzt[nzsub[k]]++;
   }
   xlnzt[1] = 1;
   for (i=1; i<=n; i++) xlnzt[i+1] = xlnzt[i] + nzt[i];

   /* Transpose matrix to order column indexes */
   transpose(n,xlnz,nzsub,lnz,xlnzt,nzsubt,lnzt,nzt);
   mat_free(A);
   *A=*At;

   /* Reclaim memory */
   free(nzt);
}

static void setlinkpos(TSystemMatrix *mat, TLinkPos *linkpos, const int row[], const int order[],
      const TListOfLists *edge_loops, const TListOfLists *loops, const TAutoValves *autovlv)
{
   int ncoeffs=mat->ncoeffs;
   int *nzsub=mat->nzsub;
   int nautov = autovlv->nautov;
   int nloops = (int) loops->size()-1;
   int nloops_noprv = nloops-nautov;       /* num loops excluding prv paths */
   std::vector<int> **lnz=mat->lnz, *plinks;
   std::vector<int> vadj_st(1,0), vadj_links;
   std::vector<double*> vadj_pos(1, (double*) NULL);
   typedef std::vector<std::vector<double *> > Tpcoeffs;
   Tpcoeffs prvloop_coeffs;
   prvloop_coeffs.resize(nautov+1);
   int k, iupd;
   unsigned ilink;
   
   mat_allocnz(mat);
   iupd=0;
   vadj_st.push_back(0);
   for (k=1; k<=ncoeffs; k++) {
      plinks=lnz[k];
      /* Set list of links and position of non-zero for adjacency */
      if (plinks != NULL) {
         /* Non-zero element not created by symbolic decomposition */
         for (ilink=0; ilink<plinks->size(); ilink++) {
            vadj_links.push_back((*plinks)[ilink]);
            iupd++;
         }
         vadj_pos.push_back(&(mat->Aij[k]));
         vadj_st.push_back(iupd);
         /* If column corresponds to a PRV path, add element position to the list of
          * coeffs for the PRV */
         int jprv = order[nzsub[k]] - nloops_noprv;
         if (jprv>0)
            prvloop_coeffs[jprv].push_back(&mat->Aij[k]);
      }
   }

   /* Set links corresponding to non-zero elems in PCV equations (matrix A21 and vector b2) */
   #if !PRVMETH_SYM
      mat_setprv_nosym(mat, row, edge_loops, loops, autovlv, vadj_st, vadj_links, vadj_pos);
   #endif

   lpos_init(linkpos, vadj_pos, vadj_st, vadj_links, prvloop_coeffs, mat, row);
   mat_free_lnz(mat);
}   


/******************************************************************************/

/* Private functions */
static void set_loop_base(TListOfLists &loops, TAutoValves &autovlv, int *looptanks);
static void shortest_path(int orig, int dest,  const int link_visited[], std::vector<int> *ploop,
         int iloop, int *looptanks);
static void min_spanning_tree();
static void minpath_spanning_tree();
static void set_edge_loops(TListOfLists &edge_loops, const TListOfLists &loops, int nloops);
static void set_loop_adj(TAdj *adj, const TListOfLists &edge_loops);
static int  buildlists(int paraflag);
static int  paralink(int i, int j, int k);
static void  xparalinks();
static void  freelists();
static void save_matrix(const char *fname, int triang, int pattern, int reord);
static void loops_print_as_spmatrix(TListOfLists &loops, const char *fname);

/* Private variables */
static int      *s_Ndx,        /* Index of link's coeff. in Aij       */
                *s_Order,      /* Loop-to-row of A                    */
                *s_Row;        /* Row-to-loop of A                    */
static TSystemMatrix s_mat;    /* Non-zero structure of the matrix */
static TLinkPos s_linkpos;     /* Positions, in the matrix and the RHS vector, of each link */
static TAutoValves s_autovlv;  /* PRV index information */
static int *s_looptanks;       /* Tanks for each loop */
static int *s_rowdist_fst;           /* First row of each thread */
static int *s_adjdist_fst;           /* First adjacency of each thread */

void load_balance(int **thread_blk_begin, const int blk_start[], const int nblk, int nthreads)
{
   *thread_blk_begin = (int *) malloc((nthreads+1)*sizeof(int));

   #ifdef _OPENMP
      int ne = blk_start[nblk+1];                   /* Number of elements to distribute */
      int neth = ne/nthreads;                       /* Min num. of elements per thread */
      int k = 1;
      (*thread_blk_begin)[0] = k;
      for (int ithread=0; ithread<nthreads; ithread++) {
         int extra2 = MIN(ithread+1,ne%nthreads);
         int last = neth*(ithread+1)+extra2;
         while (blk_start[k]<last) k++;
         if (k>2 && blk_start[k]-last > last-blk_start[k-1]) k--;
         (*thread_blk_begin)[ithread+1] = k;
      }
      #ifdef DEBUG
      printf("\nBalanceo de carga:\n");
      printf("%d elementos agrupados en %d bloques\n", ne, nblk);
      printf("Primer elemento de cada bloque:\n");
      for (int i=1; i<=nblk+1; i++)
         printf("  Bloque %d: Elem %d\n", i, blk_start[i]);
      printf("Numero medio de elementos por hilo: %.2f\n", (float)ne/nthreads);
      printf("Bloques para cada hilo:\n");
      for (int i=0; i<nthreads; i++)
         printf("  Hilo %d. Bloques: %4d-%4d. Elementos: %5d\n", i, (*thread_blk_begin)[i], (*thread_blk_begin)[i+1],
                  blk_start[(*thread_blk_begin)[i+1]]-blk_start[(*thread_blk_begin)[i]]);
      #endif
   #else
      (*thread_blk_begin)[0] = 1;
      (*thread_blk_begin)[1] = nblk+1;
   #endif
}

extern "C" int get_link_between_nodes(int u, int v)
{
   Padjlist alink;

   for (alink=Adjlist[v]; alink!=NULL; alink=alink->next)
      if (alink->node==u) return alink->link;
   return 0;
}


extern "C" void writeVector(const double v[], int n, FILE *f)
{
   int i;
   for (i=1; i<=n; i++)
      fprintf(f, "%.16e\n", v[i]);
}

extern "C" void writeVectorChar(const char v[], int n, FILE *f)
{
   int i;
   for (i=1; i<=n; i++)
      fprintf(f, "%d\n", v[i]);
}

extern "C" void readVector(double v[], int n, char *fname)
{
   FILE *f;
   int i;

   if (fname==NULL) f=stdin;
   else {
      f = fopen(fname, "r");
      assert(f!=NULL);
   }
   for (i=1; i<=n; i++)
      fscanf(f, "%lf\n", &v[i]);

   if (fname!=NULL) fclose(f);
}



extern "C" void loops_init()
/*
 *  Define a set of independent loops. Determine matrix structure, initialize linear solver
*/
{
   TListOfLists loops;
   TListOfLists edge_loops;
   TAdj loop_adj;
   int nloops, errcode = 0;
   time_L2(double tcur=LastTPoint);

   /* Build node-link adjacency lists */
   ERRCODE(buildlists(TRUE));
//   if (!errcode)
//      xparalinks();    /* Remove parallel links */

   time_L2(t_set_fromlast(Tm_hyd_buildlists));

   autov_init(&s_autovlv);
   s_looptanks = (int *) calloc(2*(Nlinks-Njuncs+s_autovlv.nautov+1), sizeof(int));

   /* Define a set of independent loops (a loop base) */
   set_loop_base(loops, s_autovlv, s_looptanks);
   int nloops_ext = Nlinks-Njuncs+s_autovlv.nautov;
   #if !PRVMETH_SYM
      nloops = Nlinks-Njuncs;
      Hyd_mat_ndense = s_autovlv.nautov;
   #else
      nloops = nloops_ext;
      Hyd_mat_ndense = 0;
   #endif
   Hyd_mat_n = nloops;

#ifdef DEBUG
   printf("\n%d loops defined\n", (int) loops.size()-1);
   printf("\n%d of them are PRV/PSV paths defined\n", (int) s_autovlv.nautov);
   print_lists(loops);
   printf("\nTanks in each loop:\n");
   for (int i=1; i<=nloops; i++)
      if (s_looptanks[2*i]!=0 || s_looptanks[2*i+1]!=0)
         printf("%d. %d-%d\n", i, s_looptanks[2*i], s_looptanks[2*i+1]);
   printf("\nPRV indices:\n [");
   for (int iv=1; iv<=s_autovlv.nautov; iv++)
      printf("%d, ", s_autovlv.avlink[iv]);
   printf("]\n");
   printf("\nSpanning tree: node -> link to parent\n");
   for (int i=1; i<=Nnodes; i++)
      printf("%d -> %d\n", i, Nodeparent[i]);
   printf("\nNode order\n");
   for (int i=1; i<=Njuncs; i++)
      printf("%d -> %d\n", i, Nodeorder[i]);
#endif

   time_L2(t_set_fromlast(Tm_hyd_setloops));

   /* For each link, obtain the list of loops it belongs to */
   set_edge_loops(edge_loops, loops, nloops);
#ifdef DEBUG
   printf("\nloops for each edge:\n");
   print_lists(edge_loops);
#endif

   /* Build loop adjacency lists */
   adj_init(&loop_adj, nloops, Nlinks);
   set_loop_adj(&loop_adj, edge_loops);
   Hyd_mat_nnz1 = loop_adj.ncoeffs+loop_adj.n;
#ifdef DEBUG
   printf("\nLoop adjacencies:\n");
   adj_print(&loop_adj);
#endif

   time_L2(t_set_fromlast(Tm_hyd_loopadj));

   /* Loop reordering and symbolic factorization on loop adjacency lists*/
   s_Row = (int *) malloc((nloops_ext+1)*sizeof(int));
   assert(s_Row!=NULL);
   s_Order = (int *) malloc((nloops_ext+1)*sizeof(int));
   assert(s_Order!=NULL);
   reorderloops(&loop_adj, s_Row, s_Order);
   #if !PRVMETH_SYM
      for (int i=Nlinks-Njuncs+1; i<=nloops_ext; i++)
         s_Row[i]=s_Order[i]=i;
   #endif
   Hyd_mat_nnz2 = loop_adj.ncoeffs+loop_adj.n;

#ifdef DEBUG
   printf("\nLoop adjacencies after symbolic decomposition:\n");
   adj_print(&loop_adj);
   for (int i=1; i<=Nlinks-Njuncs; i++)
      printf("Row: %d: corresponds to loop: %d\n", i, s_Order[i]);
#endif

   time_L2(t_set_fromlast(Tm_hyd_reord));

   /* Allocate matrix and RHS vector */
   storesparse(&s_mat, &loop_adj, s_Row, s_Order, (int) loops.size()-1);
   ordersparse(&s_mat);
   /* Set position of links and tanks in the matrix and RHS vector */
   setlinkpos(&s_mat, &s_linkpos, s_Row, s_Order, &edge_loops, &loops, &s_autovlv);
   adj_free(&loop_adj);

#ifdef DEBUG
   printf("\nStructure of reordered matrix, after symbolic decomp, in CSC format:\n");
   mat_print_A11(&s_mat);
   printf("\nPositions of each link on the matrix:\n");
   lpos_print(&s_linkpos, &s_autovlv);
#endif

   time_L2(t_set_fromlast(Tm_hyd_storesp));

   /* Build static lists for loop-link correspondance and reorder loops */
   store_listoflists_reordered(loops, &LoopLinks_start, &LoopLinks_links, s_Order);
   store_listoflists(edge_loops, &LinkLoops_start, &LinkLoops_loops);
   /* Update order of loops in LinkLoops
    * Also separate links in two groups: looped and non-looped */
   Nlinks_looped=0;
   int i2=Nlinks+1;
   Links_looped = (int *) malloc((Nlinks+1)*sizeof(int));
   assert(Links_looped!=NULL);
   for (int i=1; i<=Nlinks; i++) {
      int ini=LinkLoops_start[i];
      int fin=LinkLoops_start[i+1];
      #ifdef SEPARATE_LINKS   // Separate links (looped and non-looped)
      if (ini<fin)
         Links_looped[++Nlinks_looped]=i;
      else
         Links_looped[--i2]=i;
      #else                   // Do not separate links
         Links_looped[++Nlinks_looped]=i;
      #endif
      for (int j=ini; j<fin; j++) {
         int il = LinkLoops_loops[j];
         if (il>0) LinkLoops_loops[j] =  s_Row[il];
         else      LinkLoops_loops[j] = -s_Row[-il];
      }
   }
   assert(Nlinks_looped+1==i2);
   #ifdef DEBUG
   printf("%d links in loops. %d not in loops\n", Nlinks_looped, Nlinks-Nlinks_looped);
   #endif

   /* Update order of loops in s_looptanks */
   int *aux = s_looptanks;
   s_looptanks = (int *) malloc(2*(nloops_ext+1)*sizeof(int));
   for (int irow=0; irow<=nloops_ext; irow++) {
      int il = s_Order[irow];
      s_looptanks[2*irow] = aux[2*il];
      s_looptanks[2*irow+1] = aux[2*il+1];
   }
   free(aux);

   /* Preprocessing for load balance in linear system update */
   load_balance(&s_rowdist_fst, LoopLinks_start, nloops, Nthreads);
   load_balance(&s_adjdist_fst, s_linkpos.adj_st, s_linkpos.adj_n, Nthreads);

   DQl = (double *) malloc((nloops+1)*sizeof(double));
   assert(DQl!=NULL);
}


extern "C" void loops_free()
{
   Hyd_sumLoopLens = LoopLinks_start[s_mat.nloops+1];
   free(DQl);
   freelists();
   free(Adjlist);
   free(s_Order);
   free(s_Row);
   free(s_Ndx);
   mat_free(&s_mat);
   lpos_free(&s_linkpos);
   autov_free(&s_autovlv);
   free(LinkLoops_start);
   free(LinkLoops_loops);
   free(LoopLinks_start);
   free(LoopLinks_links);
   free(Links_looped);
   free(s_looptanks);
   free(Nodeparent);
   free(Nodeorder);
   free(s_rowdist_fst);
   free(s_adjdist_fst);
}


extern "C" int loops_linsolve(const double P[], double Y[], const double H[])
/*
 * Input: P: derivative of headloss for each link
 *        Y: headloss for each link
 *        H: node heads
 * Output: Ql - loop flows
 * Purpose: Set coefficients of the linear system, and solve it
*/
{
   int ret;
   int nloops = s_mat.nloops;
   double *Aii=s_mat.Aii;
   int *adj_st = s_linkpos.adj_st;
   int *adj_links = s_linkpos.adj_links;
   double **adj_pos = s_linkpos.adj_pos;
   time_L2(double tcur=LastTPoint);

   memset(Aii, 0, s_mat.buflen*sizeof(double));

   #pragma omp parallel
   {
   #ifdef _OPENMP
   int ithread = omp_get_thread_num();
   #else
   int ithread = 0;
   #endif

   /* Set off-diagonal coeffs */
   for (int iadj=s_adjdist_fst[ithread]; iadj<s_adjdist_fst[ithread+1]; iadj++) {
      double *p = adj_pos[iadj];
      for (int iupd=adj_st[iadj]; iupd<adj_st[iadj+1]; iupd++) {
         int k = adj_links[iupd];
         /* Accumulating link k coeff into non-zero for adjacency iadj */
         if (k>0) (*p) += P[k];
         else     (*p) -= P[-k];
      }
   }

   /* Set diagonal coeffs */
   for (int il=s_rowdist_fst[ithread]; il<s_rowdist_fst[ithread+1]; il++) {
      double *pb = &mat_b(&s_mat, il);
      for (int ilink=LoopLinks_start[il]; ilink<LoopLinks_start[il+1]; ilink++) {
         int k = LoopLinks_links[ilink];
         if (k>0) {
            Aii[il] += P[k];
            (*pb) -= Y[k];
         }
         else {
            Aii[il] += P[-k];
            (*pb) += Y[-k];
         }
      }
      /* Add contribution of tanks to RHS vector */
      /* Note: We assume that PRV paths go from the PRV to the tank, not the
       * other way around */
      if (/*s_looptanks[2*il]!=0 || */s_looptanks[2*il+1]!=0)
         (*pb) += H[s_looptanks[2*il]]-H[s_looptanks[2*il+1]];
   }
   } /* end of parallel */

   time_L2(t_set_fromlast(Tm_ncoefsys));

   #if !PRVMETH_SYM
      mat_setprv_nosym_conns(&s_mat, &s_linkpos, &s_autovlv, s_looptanks);
   #else
      mat_setprv_sym_conns(&s_mat, &s_linkpos, &s_autovlv, s_looptanks);
   #endif

   #ifdef DEBUG
      printf("\nMatriz:\n");
      linsys_print(&s_mat);
   #endif

   #if PRVMETH_SYM
      /* Solve linear system */
      ret = linsolve(nloops, s_mat.Aii, s_mat.Aij, s_mat.xlnz, s_mat.nzsub, s_mat.b-1);
   #else
   {
      int nautov = s_autovlv.nautov;
      int *avlink = s_autovlv.avlink;

      if (nautov==0) {
         /* Solve linear system */
         ret = linsolve(nloops, s_mat.Aii, s_mat.Aij, s_mat.xlnz, s_mat.nzsub, s_mat.b-1);
      }
      else {
         /* Solve linear system A11*X = B for X, where B = [ A12 b1 ]
          * A12 and b1 are overwritten with X */
         ret = linsolve_mult(nloops, nautov+1, s_mat.Aii, s_mat.Aij, s_mat.xlnz, s_mat.nzsub, s_mat.Ab);
      #ifdef DEBUG
         printf("\nMatriz despues de linsolve:\n");
         linsys_print(&s_mat);
      #endif

         /* Compute M := -Ab2+A21*X, where Ab2 = [A22 b2]
          * A22 and b2 are overwritten with M */
         mydgemm(nautov, nautov+1, nloops, s_mat.A21, nloops, s_mat.Ab, nautov+1,
                 &s_mat.Ab(nloops+1,1), nautov+1);
      #ifdef DEBUG
         printf("\nMatriz despues de dgemm:\n");
         linsys_print(&s_mat);
      #endif


         /* Solve A22*y=b2 for y
          * b2 is overwritten with y */
         mydgesv(nautov, &s_mat.Ab(nloops+1,1), nautov+1, s_mat.b2, nautov+1);
      #ifdef DEBUG
         printf("\nMatriz despues de dgesv:\n");
         linsys_print(&s_mat);
      #endif

         /* Compute loop flow corrections:
          * Operation v := b1-A12*b2
          * b1 is overwritten with v */
         mydgemv(nloops, nautov, s_mat.Ab, nautov+1, s_mat.b2, nautov+1, s_mat.b, nautov+1);
      #ifdef DEBUG
         printf("\nMatriz final:\n");
         linsys_print(&s_mat);
      #endif
         /* Update headloss for active PRV */
         for (int i=1; i<=nautov; i++) {
            int k = avlink[i];
            if (LinkStatus[k]==ACTIVE)
               Y[k] = s_mat.b2(i);
         }
      }
   }
   #endif

   /* Copy solution vector */
   for (int i=1; i<=nloops; i++)
      DQl[i] = mat_b(&s_mat,i);

#ifdef DEBUG
   printf("\nVector de caudales correctores de mallas\n");
   writeVector(DQl, nloops, stdout);
   printf("\nVector de perdidas de carga actualizado\n");
   writeVector(Y, Nlinks, stdout);
#endif

   time_L2(t_set_fromlast(Tm_linsolve));

   return s_Order[ret];
}


extern "C" double loops_flowcorrection(int k, double dq0)
/* Compute flow correction for link k */
{
   int kpos, iloop;

   for (kpos=LinkLoops_start[k]; kpos<LinkLoops_start[k+1]; kpos++) {
      iloop = LinkLoops_loops[kpos];
      if (iloop>0) dq0 += DQl[iloop];
      else         dq0 -= DQl[-iloop];
   }

   return dq0;
}


extern "C" void loops_write_network(const char *fname)
/* Write network as a matlab/octave script */
{
   FILE *f;
   int i, j, k, ilink, v, p;
   int nloops_ext=Nlinks-Njuncs+s_autovlv.nautov;

   if (fname==NULL) f=stdout;
   else             f = fopen(fname, "w");

   /* Simulation constants and parameters */
   fprintf(f, "net.consts = set_consts();");
   fprintf(f, "net.consts.formflag = %d;\n", Formflag);
   fprintf(f, "net.consts.RQtol = %.16g;\n", RQtol);
   fprintf(f, "net.consts.Hacc = %.16g;\n", Hacc);

   /* Link-node connections */
   fprintf(f, "net.A12 = sparse(%d,%d);\n", Nlinks, Njuncs);
   fprintf(f, "net.A10 = sparse(%d,%d);\n", Nlinks, Ntanks);
   for (k=1; k<=Nlinks; k++) {
      if (Link[k].N1<=Njuncs) fprintf(f, "   net.A12(%d,%d)=%d;\n",k,Link[k].N1,-1);
      else                    fprintf(f, "   net.A10(%d,%d)=%d;\n",k,Link[k].N1-Njuncs,-1);
      if (Link[k].N2<=Njuncs) fprintf(f, "   net.A12(%d,%d)=%d;\n",k,Link[k].N2,1);
      else                    fprintf(f, "   net.A10(%d,%d)=%d;\n",k,Link[k].N2-Njuncs,1);
   }
   fprintf(f, "\n");

   /* Tank heads */
   fprintf(f, "net.ht = [\n");
   for (j=Njuncs+1; j<=Nnodes; j++)
      fprintf(f, "   %.16g\n", NodeHead[j]);
   fprintf(f, "];\n\n");

   /* Demands */
   fprintf(f, "net.c = [\n");
   for (j=1; j<=Njuncs; j++)
      fprintf(f, "   %.16g\n", NodeDemand[j]);
   fprintf(f, "];\n\n");

   /* Elevations */
   fprintf(f, "net.elv = [\n");
   for (j=1; j<=Njuncs; j++)
      fprintf(f, "   %.16g\n", Node[j].El);
   fprintf(f, "];\n\n");

   /* Loop-link connections */
   fprintf(f, "net.M31 = sparse(%d,%d);\n", nloops_ext, Nlinks);
   for (i=1; i<=nloops_ext; i++) {
      for (k=LoopLinks_start[i]; k<LoopLinks_start[i+1]; k++) {
         ilink = LoopLinks_links[k];
         if (ilink>0) {
            j=ilink;
            v=1;
         }
         else {
            j=-ilink;
            v=-1;
         }
         fprintf(f, "net.M31(%d,%d)=%d;\n", i, j, v);
      }
   }
   fprintf(f, "\n");

   /* Links */
   fprintf(f, "net.link = cell(%d,1);\n", Nlinks);
   fprintf(f, "net.state = zeros(%d,1);\n", Nlinks);
   for (k=1; k<=Nlinks; k++) {
      switch (Link[k].Type)
      {
         case PIPE:
         case CV:
            fprintf(f, "net.link{%d} = pipe(%.16g,%.16g,%.16g,%.16g,net.consts);\n",
                  k, Link[k].Len, Link[k].Kc, Link[k].Diam, Link[k].Km);
            break;
         case PUMP:
            p = PUMPINDEX(k);
            fprintf(f, "net.link{%d} = pump(%.16g,%.16g,%.16g,%.16g,net.consts);\n",
                  k, Pump[p].H0, Pump[p].R, LinkSetting[k], Pump[p].N);
            break;
         case PRV:
            fprintf(f, "net.link{%d} = prv(%.16g,%.16g,%.16g,net.consts);\n", k, Link[k].Diam, LinkSetting[k], Link[k].Km);
            break;
         case PSV:
            fprintf(f, "net.link{%d} = psv(%.16g,%.16g,%.16g,net.consts);\n", k, Link[k].Diam, LinkSetting[k], Link[k].Km);
            break;
         case FCV:
            fprintf(f, "net.link{%d} = fcv(%.16g,%.16g,%.16g,net.consts);\n", k, Link[k].Diam, LinkSetting[k], Link[k].Km);
            break;
         case TCV:
            fprintf(f, "net.link{%d} = tcv(%.16g,%.16g,%.16g,net.consts);\n", k, Link[k].Diam, LinkSetting[k], Link[k].Km);
            break;
         case PBV:
            fprintf(f, "net.link{%d} = pbv(%.16g,%.16g,%.16g,net.consts);\n", k, Link[k].Diam, LinkSetting[k], Link[k].Km);
            break;
         default:
            printf("Linea de tipo: %d\n", Link[k].Type);
            assert(0);
            break;
      }
      fprintf(f, "net.state(%d)=%d;\n", k, LinkStatus[k]);
   }
   fprintf(f, "\n");

   /* PRV links */
   fprintf(f, "net.prvs.lineas = zeros(%d,1);\n", s_autovlv.nautov);
   for (i=1; i<=s_autovlv.nautov; i++) {
      ilink = s_autovlv.avlink[i];
      fprintf(f, "   net.prvs.lineas(%d)=%d;\n", i, ilink);
   }
   fprintf(f, "\n");

   /* PRV settings */
   fprintf(f, "net.prvs.consignas = zeros(%d,1);\n", s_autovlv.nautov);
   for (i=1; i<=s_autovlv.nautov; i++) {
      ilink = s_autovlv.avlink[i];
      fprintf(f, "   net.prvs.consignas(%d)=%.16g;\n ", i, LinkSetting[ilink]);
   }
   fprintf(f, "\n");

   /* PRV rows (equations) */
   fprintf(f, "net.prvs.eqs = [\n");
   for (i=1; i<=s_autovlv.nautov; i++) {
      fprintf(f, "   %d\n", s_linkpos.prvrow[i]);
   }
   fprintf(f, "];\n\n");

   /* Initial flows */
   fprintf(f, "q0 = [\n");
   for (k=1; k<=Nlinks; k++) {
      fprintf(f, "   %.16g\n", Q[k]);
   }
   fprintf(f, "];\n\n");

   /* Emitter coeffs */
   fprintf(f, "net.ke = [\n");
   for (i=1; i<=Njuncs; i++) {
      fprintf(f, "   %.16g\n", Node[i].Ke);
   }
   fprintf(f, "];\n\n");

   if (fname!=NULL) fclose(f);
}


static void set_loop_base(TListOfLists &loops, TAutoValves &autovlv, int *looptanks)
/*
 * Obtencion de una base de ciclos:
 * A medida que se construye un arbol de expansion, cada nuevo enlace (u,v) se
 * mete en un grafo G2 . Si (u,v) cierra un ciclo en el arbol, antes de
 * meterlo en G2 se busca el camino mas corto sobre G2 que lleva de u a v.
 * Dicho camino, junto con el enlace (u,v), forma un ciclo que se introduce
 * como un nuevo elemento de la base.
 * Modifies global vars:
 * - Nodeparent
 * - Nodeorder
 * - possibly others
*/
{
   int i, s, v, lk, iloop,
      nlinks_visited=0,
      nloops = Nlinks-Njuncs;
   int *depth,
      *link_visited;
   std::deque<int> seeds;
   std::vector<int> *ploop;
   Padjlist alink;

   loops.resize(nloops+autovlv.nautov+1);
   Nodeparent = (int *) malloc((Nnodes+1)*sizeof(int));
   Nodeorder = (int *) malloc((Njuncs+1)*sizeof(int));
   depth = (int *) malloc((Nnodes+1)*sizeof(int));
   link_visited = (int *) malloc((Nlinks+1)*sizeof(int));

   for (i=1; i<=Njuncs; i++) depth[i]=-1;
   for (i=1; i<=Nlinks; i++) link_visited[i]=0;
   for (i=Njuncs+1; i<=Nnodes; i++) {
      seeds.push_back(i);
      depth[i]=0;
      Nodeparent[i]=0;
   }

   i=0;
   iloop = 0;                                      /* Current loop */
   while (nlinks_visited<Nlinks) {
      s = seeds.front();
      seeds.pop_front();
      for (alink=Adjlist[s]; alink!=NULL; alink=alink->next) {
         v = alink->node;
         lk = alink->link;
         if (depth[v]==-1) {                       /* Node not yet visited */
            Nodeparent[v]=lk;
            Nodeorder[++i] = v;
            seeds.push_back(v);
            depth[v]=depth[s]+1;
            link_visited[lk]=1;
            nlinks_visited++;
         }
         else if (!link_visited[lk]) {             /* We have a new loop */
            iloop++;
            ploop = &loops[iloop];
            if (Link[lk].N1==s)
               ploop->push_back(lk);
            else
               ploop->push_back(-lk);
            shortest_path(v, s, link_visited, ploop, iloop, looptanks);
            link_visited[lk]=1;
            nlinks_visited++;
         }
      }
   }

   /* Set paths for pressure controling valves */
   for (int iv=1; iv<=autovlv.nautov; iv++) {
      int cn = autov_get_link_control_node(autovlv.avlink[iv]);
      /* Build shortest path to any tank */
      shortest_path(cn, -1, link_visited, &loops[nloops+iv], nloops+iv, looptanks);
   }

   free(link_visited);
}


static void shortest_path(int orig, int dest,  const int link_visited[], std::vector<int> *ploop,
         int iloop, int *looptanks)
/* Find shortest path from node orig to node dest, using only visited links
 * The list of links of the path is added to the loop vector pointed to by ploop
 * looptanks (tanks for each loop) is updated if necessary
 * - dest: -1 indicates any tank/reservoir
*/
{
   std::deque<int> seeds;
   int *last_link;                 /* lask link in the path to each node */
   Padjlist alink;
   int i, s, v, lk;
   int dest_arg=dest;                    /* Destination argument */

   last_link = (int *) malloc((Nnodes+1)*sizeof(int));
   for (i=1; i<=Nnodes; i++) last_link[i]=0;
   if (dest_arg==-1) dest=Njuncs+1;

   seeds.push_back(orig);
   v=orig;
   while (v!=dest) {
      s = seeds.front();
      seeds.pop_front();
      for (alink=Adjlist[s]; alink!=NULL; alink=alink->next) {
         lk = alink->link;
         if (link_visited[lk]) {
            v = alink->node;
            if (!last_link[v]) {                   /* Node v not yet reached */
               last_link[v]=lk;
               seeds.push_back(v);
               if (v>Njuncs) {                     /* Node v is a tank */
                  /* push all the other tanks into the list of seeds */
                  for (i=Njuncs+1; i<=Nnodes; i++) {
                     if (i!=v) {
                        last_link[i]=-v;           /* Fictitious link from tank i to tank v */
                        seeds.push_back(i);
                        if (i==dest) {
                           v=i;
                           break;
                        }
                     }
                  }
               }
            }
            if (v==dest) break;
         }
      }
   }

   /* Go backwards, from dest to orig, and get the sequence of links traversed */
   while (v!=orig) {
      lk = last_link[v];
      if (lk>0) {
         if (Link[lk].N2==v) {
            ploop->push_back(lk);
            v = Link[lk].N1;
         }
         else {
            ploop->push_back(-lk);
            v = Link[lk].N2;
         }
      }
      else {                                  /* Go from tank v to tank -lk */
         if (dest_arg==-1)
            dest=-lk;         /* Actual destination is the first tank reached */
         else {
            looptanks[2*iloop] = v;
            looptanks[2*iloop+1] = -lk;
         }
         v = -lk;
      }
   }
   if (dest_arg==-1)
      looptanks[2*iloop+1] = dest;

   free(last_link);
}


double *s_cost = NULL;

int compare_cost(int n1, int n2)
{
   return s_cost[n1]>s_cost[n2];
}

void heap_pop(int *pfirst, int *plast, int *node_pos, const double *cost)
{
   int node_aux, len, left, i, i2;
   int *p;
   plast--;
   len = (int) (plast-pfirst);
   node_aux = *plast;
   p = pfirst-1;
   i = 1;
   left = 2;
   while (left<=len) {
      /* set i2 <- position of the child with minimum cost */
      if (left+1>len || cost[p[left]]<cost[p[left+1]])
         i2 = left;
      else
         i2 = left+1;
      if (cost[node_aux]<=cost[p[i2]]) break;
      /* move element up from position i2 to position i */
      p[i] = p[i2];
      node_pos[p[i2]] = i;
      /* descend */
      i = i2;
      left = 2*i;
   }
   p[i] = node_aux;
   node_pos[node_aux] = i;
}

void heap_promote_node(int *pfirst, int *plast, int *node_pos, const double *cost, int k)
{
   int i, parent, node_aux;
   int *p = pfirst-1;
   node_aux = p[k];        /* Node to be promoted */
   i=k;
   parent = i/2;
   while (parent>=1 && cost[p[parent]]>cost[node_aux]) {
      /* Move element down from position parent to position i */
      p[i] = p[parent];
      node_pos[p[parent]] = i;
      /* go up */
      i = parent;
      parent = i/2;
   }
   p[i] = node_aux;
   node_pos[node_aux] = i;
}

#if 0
static void min_spanning_tree()
/*
 * Build a minimum-resistance spanning tree using Prim's algorithm
 * The root of the tree is the set of tanks, as if they were a single node
 * Modifies global variables:
 * - Nodeparent
 * - Nodeorder
 * - possibly others
*/
{
   int i, s, v, lk;
   int *node_in_tree,
      *node,        /* heap of nodes */
      *node_pos;    /* position of each node in the heap */
   int *pfirstnode, *plastnode;
   /* cost: for each node i, cost[i] is the resistance of the less-resistant link
    * connecting the node i to any node in the current spanning tree */
   double *cost, link_R;
   Padjlist alink;

   node_in_tree  = (int *) malloc((Nnodes+1)*sizeof(int));
   Nodeparent = (int *) malloc((Nnodes+1)*sizeof(int));
   node   = (int *) malloc((Nnodes+1)*sizeof(int));
   node_pos = (int *) malloc((Nnodes+1)*sizeof(int));
   cost   = (double *) malloc((Nnodes+1)*sizeof(double));
   Nodeorder = (int *) malloc((Njuncs+1)*sizeof(double));
   s_cost = cost;

   for (i=1; i<=Nnodes; i++) {
      node_in_tree[i]=0;
      Nodeparent[i]=0;
      node[i]=i;
      cost[i]=INFINITY;
   }
   for (i=Njuncs+1; i<=Nnodes; i++) {
      cost[i]=0;
   }

   pfirstnode = &node[1];
   plastnode  = &node[Nnodes+1];
   std::make_heap(pfirstnode, plastnode, compare_cost);
   /* set initial position of nodes in the heap */
   for (i=1; i<=Nnodes; i++) node_pos[node[i]] = i;

   //printf("\nNodos:  ");
   //for (int *p=pfirstnode; p<plastnode; p++) printf("%d ", *p);
   //printf("\nCostes: ");
   //for (int *p=pfirstnode; p<plastnode; p++) printf("%f ", cost[*p]);
   //printf("\n");
   i = 0;
   while (pfirstnode!=plastnode) {
      /* Extract node with minimum cost */
      s = *pfirstnode;
      heap_pop(pfirstnode, plastnode--, node_pos, cost);
      //printf("  Extraido nodo %d\n", s);
      //printf("Nodos:  ");
      //for (int *p=pfirstnode; p<plastnode; p++) printf("%d ", *p);
      //printf("\nCostes: ");
      //for (int *p=pfirstnode; p<plastnode; p++) printf("%f ", cost[*p]);
      //printf("\n");
      node_in_tree[s] = 1;
      if (s<=Njuncs) Nodeorder[++i] = s;

      for (alink=Adjlist[s]; alink!=NULL; alink=alink->next) {
         v = alink->node;
         lk = alink->link;
         link_R = (Link[lk].Type==FCV ||
                   (Link[lk].Type==PRV && v==Link[lk].N1) ||
                   (Link[lk].Type==PSV && v==Link[lk].N2))?
                   INFINITY: Link[lk].R;
         if (!node_in_tree[v] && link_R<cost[v]) {
            /* Decrease cost of node v */
            cost[v] = link_R;
            Nodeparent[v] = lk;
            heap_promote_node(pfirstnode, plastnode, node_pos, cost, node_pos[v]);
            //printf("  Nodo %d. Nuevo coste: %f\n", v, cost[v]);
         }
      }
      //printf("Nodos:  ");
      //for (int *p=pfirstnode; p<plastnode; p++) printf("%d ", *p);
      //printf("\nCostes: ");
      //for (int *p=pfirstnode; p<plastnode; p++) printf("%f ", cost[*p]);
      //printf("\n");
   }

   free(node_in_tree);
   free(node);
   free(node_pos);
   free(cost);
}


static void minpath_spanning_tree()
/*
 * Build a minimum path spanning tree using Dijkstras's algorithm
 * The root of the tree is the set of tanks, as if they were a single node
*/
{
   int i, s, v, lk;
   int *node_in_tree, *node;
   int *pfirstnode, *plastnode;
   /* cost: for each node i, cost[i] is the sum of link resistances of current
    * path from a source node to the node i */
   double *cost, costv2;
   Padjlist alink;

   node_in_tree  = (int *) malloc((Nnodes+1)*sizeof(int));
   Nodeparent = (int *) malloc((Nnodes+1)*sizeof(int));
   node   = (int *) malloc((Nnodes+1)*sizeof(int));
   cost   = (double *) malloc((Nnodes+1)*sizeof(double));
   s_cost = cost;

   for (i=1; i<=Nnodes; i++) {
      node_in_tree[i]=0;
      Nodeparent[i]=0;
      node[i]=i;
      cost[i]=INFINITY;
   }
   for (i=Njuncs+1; i<=Nnodes; i++) {
      cost[i]=0;
   }

   pfirstnode = &node[1];
   plastnode  = &node[Nnodes+1];
   std::make_heap(pfirstnode, plastnode, compare_cost);
   while (pfirstnode!=plastnode) {
      /* Extract node with minimum cost */
      s = *pfirstnode;
      std::pop_heap(pfirstnode, plastnode--, compare_cost);
      node_in_tree[s] = 1;

      for (alink=Adjlist[s]; alink!=NULL; alink=alink->next) {
         v = alink->node;
         lk = alink->link;
         costv2 = cost[s]+Link[lk].R;
         if (!node_in_tree[v] && costv2<cost[v]) {
            /* Decrease cost of node v */
            cost[v] = costv2;
            std::make_heap(pfirstnode, plastnode, compare_cost);
            Nodeparent[v] = lk;
         }
      }
   }

   free(node_in_tree);
   free(node);
   free(cost);
}
#endif

static void set_edge_loops(TListOfLists &edge_loops, const TListOfLists &loops, int nloops)
/*
 * Fills edge_loops so that for each link index e, edge_loops[e] is the
 * list of loops to which the link belongs
 * A negative number of loop (-c) indicates that the link e and the loop c have
 * opposite directions
*/
{
   int iloop, link;

   edge_loops.resize(Nlinks+1);

   /* for each loop */
   for (iloop=1; iloop<=nloops; iloop++) {
      /* for each link in the loop */
      for (std::vector<int>::const_iterator itl=loops[iloop].begin(); itl!=loops[iloop].end(); ++itl) {
         link = *itl;
         if (link>0)
            edge_loops[link].push_back(iloop);
         else
            edge_loops[-link].push_back(-iloop);
      }
   }
}


template <typename T> static void store_listoflists(const std::vector<std::vector<T> > &ll, int **starts, T **values)
/*
 * Store a list of lists in a way that can be accessed from other plain C
 * files, i.e. without using std::vector datatype
*/
{
   int i, k, n;
   typename std::vector<std::vector<T> >::const_iterator it1;
   typename std::vector<T>::const_iterator it2;

   /* Allocate and store the start index of each 2nd level list */
   *starts = (int *) malloc((ll.size()+1)*sizeof(int));
   assert(*starts!=NULL);
   (*starts)[0] = 0;
   n=0;
   i=0;
   for (it1=ll.begin(); it1!=ll.end(); ++it1) {
      n += (int) it1->size();
      (*starts)[++i] = n;
   }

   /* Allocate and store the list elements */
   *values = (T *) malloc(n*sizeof(T));
   assert(*values!=NULL);
   k=0;
   for (it1=ll.begin(); it1!=ll.end(); ++it1)
      for (it2=it1->begin(); it2!=it1->end(); ++it2)
         (*values)[k++] = *it2;
}


template <typename T> static void store_listoflists_reordered(const std::vector< std::vector<T> > &ll, int **starts, T **values, const int order[])
/*
 * Store a list of lists in a way that can be accessed from other plain C
 * files, i.e. without using std::vector datatype
*/
{
   int k, n;
   typename std::vector<T>::const_iterator it2;

   /* Allocate and store the start index of each 2nd level list */
   *starts = (int *) malloc((ll.size()+1)*sizeof(int));
   assert(*starts!=NULL);
   (*starts)[0] = 0;
   n=0;
   for (unsigned i=0; i<ll.size(); i++) {
      n += (int) ll[order[i]].size();
      (*starts)[i+1] = n;
   }

   /* Allocate and store the list elements */
   *values = (T *) malloc(n*sizeof(T));
   assert(*values!=NULL);
   k=0;
   for (unsigned i=0; i<ll.size(); i++) {
      const std::vector<T> *pv = &ll[order[i]];
      for (it2=pv->begin(); it2!=pv->end(); ++it2)
         (*values)[k++] = *it2;
   }
}


static void set_loop_adj(TAdj *adj, const TListOfLists &edge_loops)
/*
 * Obtain the matrix structure as a set of adjacency lists
*/
{
   std::vector<int>::const_iterator iloop, jloop;
   const std::vector<int> *ploops;
   int i, iaux, jaux, ic, jc, ilink;

   for (i=1; i<=Nlinks; i++) {
      ploops = &edge_loops[i];
      /* Add a non-zero element for each pair of loops link i belongs to */
      for (iloop=ploops->begin(); iloop!=ploops->end(); ++iloop) {
         iaux=*iloop;
         if (iaux>0) ic=iaux; else ic=-iaux;
         for (jloop=iloop+1; jloop!=ploops->end(); ++jloop) {
            jaux=*jloop;
            if (jaux>0) jc=jaux; else jc=-jaux;
            /* loops ic and jc share link i -> non-zero element */
            ilink = (iaux*jaux>0)?i:-i;
            adj_addadj(adj, ic, jc, ilink, 1 /* symmetric */);
         }
      }
   }
}


static int  buildlists(int paraflag)
/*
**--------------------------------------------------------------
** Input:   paraflag = TRUE if list marks parallel links
** Output:  returns error code
** Purpose: builds linked list of links adjacent to each node
**--------------------------------------------------------------
*/
{
   int    i,j,k;
   int    pmark = 0;
   int    errcode = 0;
   Padjlist  alink;

   Adjlist = (Padjlist *) calloc(Nnodes+1,  sizeof(Padjlist));
   assert(Adjlist!=NULL);
   s_Ndx    = (int *)   calloc(Nlinks+1,  sizeof(int));
   assert(s_Ndx!=NULL);

   /* For each link, update adjacency lists of its end nodes */
   for (k=1; k<=Nlinks; k++)
   {
      i = Link[k].N1;
      j = Link[k].N2;
      if (paraflag) pmark = paralink(i,j,k);  /* Parallel link check */

      /* Include link in start node i's list */
      alink = (struct Sadjlist *) malloc(sizeof(struct Sadjlist));
      if (alink == NULL) return(101);
      if (!pmark) alink->node = j;
//      else        alink->node = 0;           /* Parallel link marker */
      else        alink->node = j;           /* Parallel link marker */
      alink->link = k;
      alink->next = Adjlist[i];
      Adjlist[i] = alink;

      /* Include link in end node j's list */
      alink = (struct Sadjlist *) malloc(sizeof(struct Sadjlist));
      if (alink == NULL) return(101);
      if (!pmark) alink->node = i;
//      else        alink->node = 0;           /* Parallel link marker */
      else        alink->node = i;           /* Parallel link marker */
      alink->link = k;
      alink->next = Adjlist[j];
      Adjlist[j] = alink;
   }
   return(errcode);
}                        /* End of buildlists */


static int  paralink(int i, int j, int k)
/*
**--------------------------------------------------------------
** Input:   i = index of start node of link
**          j = index of end node of link
**          k = link index
** Output:  returns 1 if link k parallels another link, else 0
** Purpose: checks for parallel links between nodes i and j
**
**--------------------------------------------------------------
*/
{
   Padjlist alink;
   for (alink = Adjlist[i]; alink != NULL; alink = alink->next)
   {
      if (alink->node == j)     /* Link || to k (same end nodes) */
      {
         s_Ndx[k] = alink->link;  /* Assign s_Ndx entry to this link */
         return(1);
      }
   }
   s_Ndx[k] = k;                  /* s_Ndx entry if link not parallel */
   return(0);
}                        /* End of paralink */


static void  xparalinks()
/*
**--------------------------------------------------------------
** Input:   none
** Output:  none
** Purpose: removes parallel links from nodal adjacency lists
**--------------------------------------------------------------
*/
{
   int    i;
   Padjlist  alink,       /* Current item in adjacency list */
             blink;       /* Previous item in adjacency list */

   /* Scan adjacency list of each node */
   for (i=1; i<=Nnodes; i++)
   {
      alink = Adjlist[i];              /* First item in list */
      blink = NULL;
      while (alink != NULL)
      {
         if (alink->node == 0)      /* Parallel link marker found */
         {
            if (blink == NULL)      /* This holds at start of list */
            {
               Adjlist[i] = alink->next;
               free(alink);             /* Remove item from list */
               alink = Adjlist[i];
            }
            else                    /* This holds for interior of list */
            {
               blink->next = alink->next;
               free(alink);             /* Remove item from list */
               alink = blink->next;
            }
         }
         else
         {
            blink = alink;          /* Move to next item in list */
            alink = alink->next;
         }
      }
   }
}                        /* End of xparalinks */


static void  freelists()
/*
**--------------------------------------------------------------
** Input:   none
** Output:  none
** Purpose: frees memory used for nodal adjacency lists
**--------------------------------------------------------------
*/
{
   int   i;
   Padjlist alink;

   for (i=0; i<=Nnodes; i++)
   {
      for (alink = Adjlist[i]; alink != NULL; alink = Adjlist[i])
      {
         Adjlist[i] = alink->next;
         free(alink);
      }
   }
}                        /* End of freelists */


static void print_lists(TListOfLists &lists) {
   for (int i=0; i<(int)lists.size(); ++i) {
      printf("%d. [ ", i);
      for (std::vector<int>::const_iterator it2=lists[i].begin(); it2!=lists[i].end(); ++it2)
         printf("%d ", *it2);
      printf("]\n");
   }
}


static void save_matrix(const char *fname, int triang, int pattern, int reord)
/*---------------------------------------------------------------------
** Saves (main bloc of) matrix in a file. Used for debugging purposes
** - triang: if not zero, write only lower triangular part
** - pattern: if not zero, print only non-zero pattern, otherwise, print element values
** - reord: if zero, use initial order of nodes. Otherwise use reordering
** Notas:
** - se escriben tambien los elementos creados por la descomposicion simbolica
**---------------------------------------------------------------------*/
{
   FILE *fich;
   int fila, col, k, f, c;
   int *XLNZ = s_mat.xlnz;
   int *NZSUB = s_mat.nzsub;
   int n = s_mat.nloops;
   char elemtxt[30];

   fich = fopen(fname, "w");
   assert(fich!=NULL);

   /* Elementos diagonales */
   for (k=1; k<=n; k++) {
      if (reord) f=k;
      else       f=s_Order[k];
      if (pattern) sprintf(elemtxt,"%.1f", 1.0);
      else         sprintf(elemtxt,"%.16e", s_mat.Aii[k]);
      fprintf(fich, "%d\t%d\t%s\n", f, f, elemtxt);
   }

   /* Elementos no diagonales */
   for (col=1; col<=n; col++) {
      for (k=XLNZ[col]; k<XLNZ[col+1]; k++) {
         fila = NZSUB[k];
         if (reord) {
            f=fila;
            c=col;
         }
         else {
            f=s_Order[fila];
            c=s_Order[col];
         }
         if (pattern) sprintf(elemtxt,"%.1f", 1.0);
         else         sprintf(elemtxt,"%.16e", s_mat.Aij[k]);
         fprintf(fich, "%d\t%d\t%s\n", f, c, elemtxt);
         if (!triang)
            fprintf(fich, "%d\t%d\t%s\n", c, f, elemtxt);
      }
   }

   fclose(fich);
}


static void loops_print_as_spmatrix(TListOfLists &loops, const char *fname)
/* Print sparse matrix of loop adjacencies */
{
   int i, j, v, ilink, nloops = Nlinks-Njuncs;
   std::vector<int>::const_iterator it;
   FILE *f;

   if (fname==NULL) f=stdout;
   else             f = fopen(fname, "w");

   for (i=1; i<=nloops; i++) {
      for (it=loops[i].begin(); it!=loops[i].end(); ++it) {
         ilink = *it;
         if (ilink>0) {
            j=ilink;
            v=1;
         }
         else {
            j=-ilink;
            v=-1;
         }
         fprintf(f, "%d %d %d\n", i, j, v);
      }
   }
   if (fname!=NULL) fclose(f);
}
