/*
************************************************************************

 LOOPNET -- Hydraulic & Water Quality Simulator for Water Distribution Networks
 See LICENSE and AUTHORS files

 VARS.H -- Global Variables for LOOPNET
                                                                     
************************************************************************
*/
#ifndef VARS_H
#define VARS_H

#include <stdio.h>
#include "hash.h"

EXTERN FILE     *InFile,               /* Input file pointer           */
                *OutFile,              /* Output file pointer          */
                *RptFile,              /* Report file pointer          */
                *HydFile,              /* Hydraulics file pointer      */
                *TmpOutFile;           /* Temporary file handle        */
EXTERN long     HydOffset,             /* Hydraulics file byte offset  */
                OutOffset1,            /* 1st output file byte offset  */
                OutOffset2;            /* 2nd output file byte offset  */
EXTERN char     Msg[MAXMSG+1],         /* Text of output message       */
                InpFname[MAXFNAME+1],  /* Input file name              */
                Rpt1Fname[MAXFNAME+1], /* Primary report file name     */
                Rpt2Fname[MAXFNAME+1], /* Secondary report file name   */
                HydFname[MAXFNAME+1],  /* Hydraulics file name         */
                OutFname[MAXFNAME+1],  /* Binary output file name      */
                MapFname[MAXFNAME+1],  /* Map file name                */
                TmpFname[MAXFNAME+1],  /* Temporary file name          */      //(2.00.12 - LR)
                TmpDir[MAXFNAME+1],    /* Temporary directory name     */      //(2.00.12 - LR)
                Title[MAXTITLE][MAXTLINE+1], /* Problem title            */
                ChemName[MAXID+1],     /* Name of chemical             */
                ChemUnits[MAXID+1],    /* Units of chemical            */
                DefPatID[MAXID+1],     /* Default demand pattern ID    */

/*** Updated 6/24/02 ***/
                Atime[13],             /* Clock time (hrs:min:sec)     */

                Outflag,               /* Output file flag             */      //(2.00.12 - LR)
                Hydflag,               /* Hydraulics flag              */
                Qualflag,              /* Water quality flag           */
                Reactflag,             /* Reaction indicator           */      //(2.00.12 - LR)
                Unitsflag,             /* Unit system flag             */
                Flowflag,              /* Flow units flag              */
                Pressflag,             /* Pressure units flag          */
                Formflag,              /* Hydraulic formula flag       */
                Rptflag,               /* Report flag                  */
                Summaryflag,           /* Report summary flag          */
                Messageflag,           /* Error/warning message flag   */
                Statflag,              /* Status report flag           */
                Energyflag,            /* Energy report flag           */
                Nodeflag,              /* Node report flag             */
                Linkflag,              /* Link report flag             */
                Tstatflag,             /* Time statistics flag         */
                Warnflag,              /* Warning flag                 */
                Openflag,              /* Input processed flag         */
                OpenHflag,             /* Hydraul. system opened flag  */
                SaveHflag,             /* Hydraul. results saved flag  */
                OpenQflag,             /* Quality system opened flag   */
                SaveQflag,             /* Quality results saved flag   */
                Saveflag,              /* General purpose save flag    */
                Coordflag;             /* Load coordinates flag        */
EXTERN int      MaxNodes,              /* Node count from input file   */
                MaxLinks,              /* Link count from input file   */
                MaxJuncs,              /* Junction count               */
                MaxPipes,              /* Pipe count                   */
                MaxTanks,              /* Tank count                   */
                MaxPumps,              /* Pump count                   */
                MaxValves,             /* Valve count                  */
                MaxControls,           /* Control count                */
                MaxRules,              /* Rule count                   */
                MaxPats,               /* Pattern count                */
                MaxCurves,             /* Curve count                  */
                Nnodes,                /* Number of network nodes      */
                Ntanks,                /* Number of tanks              */
                Njuncs,                /* Number of junction nodes     */
                Nlinks,                /* Number of network links      */
                Npipes,                /* Number of pipes              */
                Npumps,                /* Number of pumps              */
                Nvalves,               /* Number of valves             */
                Ncontrols,             /* Number of simple controls    */
                Nrules,                /* Number of control rules      */
                Npats,                 /* Number of time patterns      */
                Ncurves,               /* Number of data curves        */
                Nperiods,              /* Number of reporting periods  */
                Ncoeffs,               /* Number of non-0 matrix coeffs*/
                DefPat,                /* Default demand pattern       */
                Epat,                  /* Energy cost time pattern     */
                MaxIter,               /* Max. hydraulic trials        */
                ExtraIter,             /* Extra hydraulic trials       */
                TraceNode,             /* Source node for flow tracing */
                PageSize,              /* Lines/page in output report  */
                CheckFreq,             /* Hydraulics solver parameter  */
                MaxCheck;              /* Hydraulics solver parameter  */
EXTERN double   Ucf[MAXVAR],           /* Unit conversion factors      */
                Ctol,                  /* Water quality tolerance      */
                Htol,                  /* Hydraulic head tolerance     */
                Qtol,                  /* Flow rate tolerance          */
                RQtol,                 /* Flow resistance tolerance    */
                Hexp,                  /* Exponent in headloss formula */
                Qexp,                  /* Exponent in orifice formula  */
                Dmult,                 /* Demand multiplier            */
                Hacc,                  /* Hydraulics solution accuracy */
                DampLimit,             /* Solution damping threshold   */      //(2.00.12 - LR)
                BulkOrder,             /* Bulk flow reaction order     */
                WallOrder,             /* Pipe wall reaction order     */
                TankOrder,             /* Tank reaction order          */
                Kbulk,                 /* Global bulk reaction coeff.  */
                Kwall,                 /* Global wall reaction coeff.  */
                Climit,                /* Limiting potential quality   */
                Rfactor,               /* Roughness-reaction factor    */
                Diffus,                /* Diffusivity (sq ft/sec)      */
                Viscos,                /* Kin. viscosity (sq ft/sec)   */
                SpGrav,                /* Specific gravity             */
                Ecost,                 /* Base energy cost per kwh     */
                Dcost,                 /* Energy demand charge/kw/day  */
                Epump,                 /* Global pump efficiency       */
                Emax,                  /* Peak energy usage            */
                Dsystem,               /* Total system demand          */
                Wbulk,                 /* Avg. bulk reaction rate      */
                Wwall,                 /* Avg. wall reaction rate      */
                Wtank,                 /* Avg. tank reaction rate      */
                Wsource;               /* Avg. mass inflow             */
EXTERN long     Tstart,                /* Starting time of day (sec)   */
                Hstep,                 /* Nominal hyd. time step (sec) */
                Qstep,                 /* Quality time step (sec)      */
                Pstep,                 /* Time pattern time step (sec) */
                Pstart,                /* Starting pattern time (sec)  */
                Rstep,                 /* Reporting time step (sec)    */
                Rstart,                /* Time when reporting starts   */
                Rtime,                 /* Next reporting time          */
                Htime,                 /* Current hyd. time (sec)      */
                Qtime,                 /* Current quality time (sec)   */
                Hydstep,               /* Actual hydraulic time step   */
                Rulestep,              /* Rule evaluation time step    */
                Dur;                   /* Duration of simulation (sec) */
EXTERN SField   Field[MAXVAR];         /* Output reporting fields      */

/* Array pointers not allocated and freed in same routine */
EXTERN char     *LinkStatus,           /* Link status                  */
                *OldStat;              /* Previous link/tank status    */
EXTERN double   *NodeDemand,           /* Node actual demand           */
                *NodeQual,             /* Node actual quality          */
                *E,                    /* Emitter flows                */
                *LinkSetting,          /* Link settings                */
                *Q,                    /* Link flows                   */
                *DQl,                  /* Loop flow corrections        */
                *DQ,                   /* Link flow corrections        */
                /* TODO: DQ solo se usa para las valvulas PRV/PSV, para guardar
                 * en loops_linsolve un caudal que luego se debe recuperar
                 * en newflows. Se podria usar un array de longitud igual
                 * al numero de PRV/PSV, o bien usar el array X */
                *Qblc,                 /* Balanced flows               */
                *PipeRateCoeff,        /* Pipe reaction rate           */
                *X,                    /* General purpose array        */
                *P,                    /* Inverse headloss derivatives        */
                *Y,                    /* Flow correction factors             */
                *TempQual;             /* General purpose array for water quality        */
EXTERN double   *NodeHead;                    /* Node heads                   */
EXTERN double *QTankVolumes;
EXTERN double *QLinkFlow;
EXTERN STmplist *Patlist;              /* Temporary time pattern list  */ 
EXTERN STmplist *Curvelist;            /* Temporary list of curves     */
EXTERN Spattern *Pattern;              /* Time patterns                */
EXTERN Scurve   *Curve;                /* Curve data                   */
EXTERN Scoord   *Coord;                /* Coordinate data              */
EXTERN Snode    *Node;                 /* Node data                    */
EXTERN Slink    *Link;                 /* Link data                    */
EXTERN Stank    *Tank;                 /* Tank data                    */
EXTERN Spump    *Pump;                 /* Pump data                    */
EXTERN Svalve   *Valve;                /* Valve data                   */
EXTERN Scontrol *Control;              /* Control data                 */
EXTERN ENHashTable  *NodeHashTable, *LinkHashTable;            /* Hash tables for ID labels    */
EXTERN Padjlist *Adjlist;              /* Node adjacency lists         */
EXTERN double _relativeError;
EXTERN int _iterations; /* Info about hydraulic solution */

EXTERN int OptDebugQH;

EXTERN int Nthreads;

/*
** NOTE: Hydraulic analysis of the pipe network at a given point in time
**       is done by repeatedly solving a linearized version of the 
**       equations for conservation of flow & energy:
**
**           A*DQl = F
**
**       where DQl = vector of flow corrections (unknowns) at each loop,
**             F = vector of right-hand side coeffs.
**             A = square matrix of coeffs.
**       and both A and F are updated at each iteration until there is
**       negligible change in pipe flows.
**
**       Each row (or column) of A corresponds to a loop in the pipe
**       network. Each link (pipe, pump or valve) in the network has a
**       coefficent that is added to the diagonal element corresponding to 
**       each of the loops the link belongs to, and also to the off-diagonal 
**       element corresponding to every pair of such loops.
*/


/*---------------------------------------------------------------------------*/
/* LOOP SOLVER */

EXTERN  int  *Nodeparent;  /* Link number connecting to predecessor in the node tree */
EXTERN  int  *Nodeorder;

/* For each loop, list of links belonging to it (loops are indexed according to reordering) */
EXTERN int *LoopLinks_start;   /* Position in LoopLinks_links, where the list of links of 
                                * each loop begins */
EXTERN int *LoopLinks_links;   /* Lists of links of each loop. All the lists concatenated
                                * in a single one */

/* For each link, list of loops it belongs to (loops are indexed according to reordering) */
EXTERN int *LinkLoops_start;   /* Position in LinkLoops_loops, where the list of loops of 
                                * each link begins */
EXTERN int *LinkLoops_loops;   /* Lists of loops of each link. All the lists concatenated
                                * in a single one */

EXTERN int *Links_looped,      /* Links in looped part of the network, followed by links
                                * in non-looped part */
            Nlinks_looped;     /* Number of links in looped part */


/*---------------------------------------------------------------------------*/
/* Datos sobre prestaciones de la simulacion hidraulica */

#if TIMING
EXTERN double LastTPoint,
              Tm_hyd_open,
              Tm_hyd_init,        /* Time inithyd() */
              Tm_hyd_run,         /* Time runhyd() */
              Tm_hyd_next;        /* Time nexthyd() */
#endif
#if TIMING>=2
EXTERN double Tm_hyd_buildlists,
              Tm_hyd_setloops,
              Tm_hyd_loopadj,
              Tm_hyd_reord,
              Tm_hyd_storesp,
              Tm_demands,     /* Time for setting up demands */
              Tm_controls,    /* Time for setting up controls */
              Tm_netsolve,    /* Time netsolve() */
              Tm_blcflows,    /* Time balanceflows() */
              Tm_ncoefpy_pre, /* Time for computing coeffs P, Y before 
                                Newton-Raphson iterations */
              Tm_ncoefpy_it,  /* Time for computing coeffs P, Y in Newton-Raphson 
                                iterations */
              Tm_ncoefsys,    /* Time to assemble P, Y coeffs into the linear 
                                system */
              Tm_linsolve,
              Tm_newflows,    
              Tm_newheads,
              Tm_newstatus,
              Tm_chknetsv;    /* Time for checking netsolve results */
#endif

EXTERN double Hyd_maxerr,       /* Maximum relative error incurred */
              Hyd_maxres_e,     /* Maximum energy residual */
              Hyd_maxres_q;     /* Maximum flow residual */

EXTERN int    Hyd_num_nolin,     /* Number of non-linear systems solved */
              Hyd_num_lin,       /* Numero of linear systems solved */
              Hyd_mat_n,         /* Matrix rows/cols */      
              Hyd_mat_nnz1,      /* num non zeros before symbolic decomposition */
              Hyd_mat_nnz2,      /* num non zeros after symbolic decomposition */
              Hyd_mat_ndense,    /* size (rows/cols) of dense block */
              Hyd_sumLoopLens;   /* Sum of the number of links of all the loops */

#endif
