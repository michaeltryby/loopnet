## What is Loopnet?

**Loopnet** is a software for the simulation of water distribution networks based on [EPANET 2.1 from Open Water Analytics](https://github.com/OpenWaterAnalytics/EPANET).

Loopnet focuses on **speeding up the simulation**, for which purpose it diverts from EPANET 2.1 in two main aspects:

- It uses the **loop method** (or looped Newton-Raphson method) for the solution of the hydraulic simulation non-linear systems. Compared to the *Global Gradient Algorithm* used in EPANET, the loop method generally leads to significantly smaller linear systems, thus speeding up the simulation [1][2].

- It uses OpenMP to take advantage of current **multicore** processors in order to further speed up the hydraulic simulation [3].

## Features

Loopnet supports all the different types of hydraulic elements that Epanet 2.1 can manage, with the only exception of emitters. In particular, Loopnet can simulate models including any of the different types of valves, pumps and (simple and rule-based) controls. All the external functions in the Toolkit’s API are the same as in EPANET 2.1. 

## Building

You can download pre-built 32-bit/64-bit DLL libraries from [Releases](https://gitlab.com/feralber/loopnet/-/releases). Alternatively, you can build it yourself by cloning/downloading the source code of this repository and following the instructions at [BUILDING.md](BUILDING.md).

## Using the library on Windows with Epanet 2.0 GUI

On Windows, you can use the Epanet 2.0 GUI (epanet2w.exe) with Loopnet's toolkit library (DLL) instead of Epanet's own library. To do that:

1. Download the 32-bit library from [Releases](https://gitlab.com/feralber/loopnet/-/releases) or, alternatively, build a 32-bit Loopnet library following the instructions at [BUILDING.md](BUILDING.md).
2. Create an empty folder that will contain the epanet2w program and Loopnet's library. Copy into it the file `loopnetlib.dll` generated at the previous step. Rename the file as `epanet2.dll`.
3. Copy into the same folder the file `Epanet2w.exe` distributed as part of Epanet.
4. Double click on `Epanet2w.exe`. The program will be using Loopnet's DLL.

## Licence

Loopnet is distributed under the MIT license as described in the LICENSE file of this repository.

## Authors 

Loopnet was developed as part of the doctoral thesis of Fernando Alvarruiz Bermejo[4], under the supervision of Profs. Fernando Martínez Alzamora and Antonio M. Vidal Maciá. See the AUTHORS file of this repository.

## References

[1] [Alvarruiz, F., Martínez Alzamora, F., Vidal, A. M. "Improving the Efficiency of the Loop Method for the Simulation of Water Distribution Systems", Journal of Water Resources Planning and Management, Vol. 141(10), 2015](https://ascelibrary.org/doi/full/10.1061/%28ASCE%29WR.1943-5452.0000539)

[2] [Alvarruiz, F., Martínez Alzamora, F., Vidal, A. M. "Efficient Modeling of Active Control Valves in Water Distribution Systems Using the Loop Method", Journal of Water Resources Planning and Management, Vol. 144(10), 2018](https://ascelibrary.org/doi/10.1061/%28ASCE%29WR.1943-5452.0000982)

[3] [Alvarruiz, F., Martínez Alzamora, F., Vidal, A. M. “Improving the performance of water distribution systems simulation on multicore systems.” Journal of Supercomputing 73 (1): 44–56, 2017](https://doi.org/10.1007/s11227-015-1607-5)

[4] [Alvarruiz, F. "Reducción del Tiempo de Simulación de Redes de Distribución de Agua, mediante el
Método de Mallas y la Computación de Altas Prestaciones" PhD dissertation, 2016](https://riunet.upv.es/handle/10251/61764)

[5] [Alvarruiz, F., Martínez Alzamora, F., Vidal, A. M. "A Toolkit for Water Distribution Systems’ Simulation Using the Loop Method and High Performance Computing" XVIII International Conference on Water Distribution Systems, WDSA2016, 2016](https://doi.org/10.1016/j.proeng.2017.03.250)
